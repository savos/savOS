CROSS_PREFIX=i686-elf
CROSS_PREFIX64=x86_64-elf

CC=clang -target i386-pc-none
CC64=clang -target x86_64-pc-none -mcmodel=kernel

CPPC=clang -target i386-pc-none
CPPC64=clang -target x86_64-pc-none -mcmodel=kernel

AS=$(CROSS_PREFIX)-gcc
AS64=$(CROSS_PREFIX64)-gcc -mcmodel=kernel

LD=$(CROSS_PREFIX)-gcc
LD64=$(CROSS_PREFIX64)-gcc -mcmodel=kernel

OLEVEL=-O3
COMMON_FLAGS=-fno-builtin -mno-sse -mno-mmx -ffreestanding -pedantic -Wall -Wextra -Wno-unknown-pragmas -fno-omit-frame-pointer -Wno-format -Wno-unused-parameter -c -Iinclude/ $(OLEVEL) -Ilib/acpica/source/include -DSAVOS

CFLAGS=-g -std=c99 $(COMMON_FLAGS)
CPPFLAGS=-g -std=c++14 $(COMMON_FLAGS) -fno-exceptions -fno-asynchronous-unwind-tables -fno-rtti -fno-threadsafe-statics
AFLAGS=-g -c -Iinclude/
LDFLAGS=-g -ffreestanding $(OLEVEL) -nostdlib -lgcc -static-libgcc
ACPICA_FLAGS=-g -mno-sse -mno-mmx -std=c99 -ffreestanding -O2 -c -Ilib/acpica/source/include -include include/main/utils.h -DSAVOS
64FLAGS=-DX86_64 -mno-red-zone

CRTBEGIN_OBJ:=$(shell $(LD) $(LDFLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ:=$(shell $(LD) $(LDFLAGS) -print-file-name=crtend.o)
CRTBEGIN64_OBJ:=$(shell $(LD64) $(LDFLAGS) -print-file-name=crtbegin.o)
CRTEND64_OBJ:=$(shell $(LD64) $(LDFLAGS) -print-file-name=crtend.o)

BUILD=build
I386=$(BUILD)/i386
X86_64=$(BUILD)/x86_64

OBJECTS=$(I386)/objects/debug/assert.o\
	$(I386)/objects/debug/stack.o\
	$(I386)/objects/debug/kernel_elf.o\
	$(I386)/objects/display/display.o\
	$(I386)/objects/display/logdisplay.o\
	$(I386)/objects/fs/filesystem.o\
	$(I386)/objects/fs/filesystem_test.o\
	$(I386)/objects/fs/physical_mem_storage.o\
	$(I386)/objects/fs/expanse_fs.o\
	$(I386)/objects/fs/storage_worker.o\
	$(I386)/objects/hw/cmos.o\
	$(I386)/objects/hw/acpi.o\
	$(I386)/objects/hw/acpica.o\
	$(I386)/objects/hw/pci/pci.o\
	$(I386)/objects/hw/pci/ahci.o\
	$(I386)/objects/hw/pit.o\
	$(I386)/objects/hw/ps2.o\
	$(I386)/objects/hw/ps2keyboard.o\
	$(I386)/objects/hw/serial.o\
	$(I386)/objects/hw/utils.o\
	$(I386)/objects/int/exceptions.o\
	$(I386)/objects/int/idt.o\
	$(I386)/objects/int/ioapic.o\
	$(I386)/objects/int/lapic.o\
	$(I386)/objects/int/pic.o\
	$(I386)/objects/int/wrapper.o\
	$(I386)/objects/logging/log.o\
	$(I386)/objects/logging/printk.o\
	$(I386)/objects/main/asm_utils.o\
	$(I386)/objects/main/cmdline.o\
	$(I386)/objects/main/cpp.o\
	$(I386)/objects/main/cpu.o\
	$(I386)/objects/main/loader_data.o\
	$(I386)/objects/main/main.o\
	$(I386)/objects/main/multiboot.o\
	$(I386)/objects/main/panic.o\
	$(I386)/objects/main/utils.o\
	$(I386)/objects/main/vga.o\
	$(I386)/objects/mem/gdt.o\
	$(I386)/objects/mem/gdt_asm.o\
	$(I386)/objects/mem/kmem.o\
	$(I386)/objects/mem/object.o\
	$(I386)/objects/mem/page.o\
	$(I386)/objects/mem/vm.o\
	$(I386)/objects/structures/elf.o\
	$(I386)/objects/structures/list.o\
	$(I386)/objects/structures/mutex.o\
	$(I386)/objects/structures/static_list.o\
	$(I386)/objects/structures/shared_ptr.o\
	$(I386)/objects/structures/stream.o\
	$(I386)/objects/structures/unique_ptr.o\
	$(I386)/objects/structures/utf8.o\
	$(I386)/objects/structures/vector.o\
	$(I386)/objects/task/asm.o\
	$(I386)/objects/task/task.o\
	$(I386)/objects/test/test.o\
	$(I386)/objects/utils/text.o\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/dispatcher/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/executer/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/parser/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/utilities/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/namespace/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/tables/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/events/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/hardware/*.c))\
	$(patsubst lib/acpica/source/components/%.c, $(I386)/objects/acpica/%.o, $(wildcard lib/acpica/source/components/resources/*.c))

OBJECTS_32_ONLY=$(I386)/objects/low/loerror.o\
	$(I386)/objects/low/lomain.o\
	$(I386)/objects/low/lomultiboot.o\
	$(I386)/objects/low/ap.o\
	$(I386)/objects/low/boot.o\

OBJECTS_64=$(subst $(I386), $(X86_64), $(OBJECTS))

OBJECTS_64_ONLY=$(X86_64)/objects/low/ap_64.o\

OBJECTS_LOADER=$(I386)/objects/low/64_kernel.o\
	$(I386)/objects/low/loader_boot.o\
	$(I386)/objects/low/loader_main.o\
	$(I386)/objects/low/loerror.o\
	$(I386)/objects/low/lomultiboot.o\
	$(I386)/objects/structures/elf.o\
	$(I386)/objects/low/loader_gdt.o

# Source file rules
$(I386)/objects/%.o: src/%.cpp include/config.hpp
	@echo "[c++] $<"
	@mkdir -p $(@D)
	@$(CPPC) $(CPPFLAGS) -o $@ -imacros include/config.hpp $<
$(X86_64)/objects/%.o: src/%.cpp include/config.hpp
	@echo "[c++] $<"
	@mkdir -p $(@D)
	@$(CPPC64) $(CPPFLAGS) $(64FLAGS) -o $@ -imacros include/config.hpp $<

$(I386)/objects/%.o: src/%.c
	@echo "[c  ] $<"
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -o $@ $^
$(X86_64)/objects/%.o: src/%.c
	@echo "[c  ] $<"
	@mkdir -p $(@D)
	@$(CC64) $(CFLAGS) $(64FLAGS) -o $@ $^

$(I386)/objects/%.o: src/%.s
	@echo "[s  ] $<"
	@mkdir -p $(@D)
	@$(AS) $(AFLAGS) -o $@ $^

$(I386)/objects/%.o: src/%.S
	@echo "[S  ] $<"
	@mkdir -p $(@D)
	@$(AS) $(AFLAGS) -o $@ -imacros include/config.hpp $^
$(X86_64)/objects/%.o: src/%.S
	@echo "[S  ] $<"
	@mkdir -p $(@D)
	@$(AS64) $(AFLAGS) $(64FLAGS) -o $@ -imacros include/config.hpp $^
$(I386)/objects/low/64_kernel.o: src/low/64_kernel.S $(X86_64)/savos_kernel.bin
	@echo "[S  ] $<"
	@mkdir -p $(@D)
	@$(AS) $(AFLAGS) -o $@ -imacros include/config.hpp $<

$(I386)/objects/acpica/%.o: lib/acpica/source/components/%.c
	@echo "[c  ] (ACPICA) $<"
	@mkdir -p $(@D)
	@$(CC) $(ACPICA_FLAGS) -o $@ $^
$(X86_64)/objects/acpica/%.o: lib/acpica/source/components/%.c
	@echo "[c  ] (ACPICA) $<"
	@mkdir -p $(@D)
	@$(CC64) $(64FLAGS) $(ACPICA_FLAGS) -o $@ $^

# General building
clean:
	@echo "Erasing $(I386)..."
	@-rm -r $(I386)/*
	@echo "Erasing $(X86_64)..."
	@-rm -r $(X86_64)/*

$(I386)/savos.bin: $(I386)/objects/low/multiboot.o $(OBJECTS) $(OBJECTS_32_ONLY)
	@echo "Linking... (32b)"
	@$(LD) -T linker.ld  -o $(I386)/savos.bin $(I386)/objects/main/crti.o $(CRTBEGIN_OBJ) $^ $(CRTEND_OBJ) $(I386)/objects/main/crtn.o $(LDFLAGS)

$(X86_64)/savos_kernel.bin: $(X86_64)/objects/main/crti.o $(X86_64)/objects/main/crtn.o $(OBJECTS_64) $(OBJECTS_64_ONLY)
	@echo "Linking... (64b)"
	@$(LD64) -mcmodel=large -T linker64.ld -o $(X86_64)/savos_kernel.bin $(X86_64)/objects/main/crti.o $(CRTBEGIN64_OBJ) $(OBJECTS_64) $(OBJECTS_64_ONLY) $(CRTEND64_OBJ) $(X86_64)/objects/main/crtn.o $(LDFLAGS)

grub: $(I386)/objects/main/crti.o $(I386)/objects/main/crtn.o $(I386)/savos.bin
	@echo "Populating isodir (32b)..."
	@mkdir -p $(I386)/iso/boot/grub
	@cp grub.cfg $(I386)/iso/boot/grub/
	@cp $(I386)/savos.bin $(I386)/iso/boot/savos.bin

	@echo "Running grub-mkrescue..."
	@grub-mkrescue -o $(I386)/savos.iso $(I386)/iso

$(X86_64)/savos.bin: $(I386)/objects/main/crti.o $(I386)/objects/main/crtn.o $(X86_64)/savos_kernel.bin $(OBJECTS_LOADER)
	@echo "Linking Loader (64b)..."
	@$(LD) -T linker_loader.ld  -o $(X86_64)/savos.bin $(I386)/objects/main/crti.o $(CRTBEGIN_OBJ) $(OBJECTS_LOADER) $(CRTEND_OBJ) $(I386)/objects/main/crtn.o $(LDFLAGS)

grub64: $(X86_64)/objects/main/crti.o $(X86_64)/objects/main/crtn.o $(X86_64)/savos.bin
	@echo "Populating isodir (64b)..."
	@mkdir -p $(X86_64)/iso/boot/grub
	@cp grub.cfg $(X86_64)/iso/boot/grub/
	@cp $(X86_64)/savos.bin $(X86_64)/iso/boot/savos.bin

	@echo "Running grub-mkrescue..."
	@grub-mkrescue -o $(X86_64)/savos.iso $(X86_64)/iso

all: grub grub64

docs:
	@echo "Making documentation..."
	@-rm -r build/doc/*
	@doxygen include/Doxyfile
