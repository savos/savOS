#pragma once

#if NDEBUG
#define assert(X)
#else
#include "main/panic.hpp"

#define assert(X) (X ? (void)0 : assert::do_assert(X, #X, __LINE__, __func__, __FILE__))

/** Contains the assert method */
namespace assert {
void do_assert(const bool condition, const char *expr, const int lineno, const char *function, const char *file);
}
#endif
