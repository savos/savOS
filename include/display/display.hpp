#pragma once

#include "main/common.hpp"
#include "structures/buffer.hpp"

namespace display {
class Display {
protected:
  int cols = 0;
  int rows = 0;
  Buffer<volatile uint16_t> &buffer;

public:
  bool active = false;
  size_t id = 0;

public:
  Display(Buffer<volatile uint16_t> &buffer, size_t id);
  virtual void winch(int cols, int rows);
  virtual void enable();
  virtual void disable();
  virtual ~Display(){};
};

class TestDisplay : public Display {
public:
  TestDisplay(Buffer<volatile uint16_t> &buffer, size_t id);
  void enable() override;

private:
  void update();
};
} // namespace display
