#pragma once

#include "main/common.hpp"
#include "structures/buffer.hpp"
#include "display/display.hpp"

namespace log_display {
using namespace display;

class LogDisplay : public Display {
public:
  LogDisplay(Buffer<volatile uint16_t> &buffer, size_t id);
  void enable() override;

private:
  void update();
};

} // namespace display
