#pragma once

#include "main/common.hpp"

/** Contains methods for parsing the acpi headers
 *
 * At the current time, the only things parsed out of the tables are the madt tables.
 *
 * To populate the various structures in this module, @ref low_setup must be called from low memory, which will
 *  scan for the various tables, and then fill in the high memory versions of them. That is, low_setup must be called
 *  before @ref kernel_main, but its structures must be used after that point.
 *
 * Please note that any pointers you find in the copied structure will likely be invalid.
 */
namespace acpi {

/** Information about a processor from a MADT entry */
struct MadtProc {
  uint8_t id;
  uint8_t apic_id;
  uint32_t flags;
};

/** Information about an ioapic from a MADT entry */
struct MadtIoapic {
  uint8_t id;
  uint8_t reserved;
  addr_phys_t addr;
  addr_phys_t int_base;
};

/** Information about an interrupt service override from a MADT entry */
struct MadtIso {
  uint8_t bus_source;
  uint8_t irq_source;
  uint32_t global_int;
  uint16_t flags;
};

/** Copies of all the MADT entries for processors */
extern vector<MadtProc> procs;
/** Copies of all the MADT entries for ioapics */
extern vector<MadtIoapic> ioapics;
/** Copies of all the MADT entries for interupt service overrides */
extern vector<MadtIso> isos;

/** Number of MADT processor entries (also the number of processors in the system) */
extern uint32_t proc_count;
/** Number of MADT ioapic entries (also the number of ioapics in the system) */
extern uint32_t ioapic_count;

/** The base address of LAPIC memory as defined in the tables */
extern addr_phys_t lapic_base;

extern bool acpi_found;
extern bool inited;

void init();

void init_pci();

uint32_t get_irq_override(uint8_t source);

} // namespace acpi
