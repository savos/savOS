#pragma once

#include "main/common.hpp"
#include "structures/list.hpp"
#include "structures/unique_ptr.hpp"

namespace pci_ahci {
struct hba_port_t;
class AhciDriver;

class AhciDevice {
private:
  volatile hba_port_t *port;

public:
  AhciDevice(volatile hba_port_t *port);
  bool active();

private:
  friend AhciDriver;
  void configure();
};

extern vector<AhciDevice> devices;
} // namespace pci_ahci
