#ifndef _HPP_INTERRUPTS_IDT_
#define _HPP_INTERRUPTS_IDT_

/** @file int/idt.h
 *
 * Handles setting up the IDT, and managing interrupts.
 *
 * Also includes a structure representing the state of the CPU (as used by interrupts) and functions for adding
 *  vaules to the IDT.
 *
 * The IDT itself will be created and added by calling @ref init, and is stored statically. New entries may be added
 *  to this table using @ref install.
 *
 * The difference between a trap gate and an interrupt gate, is that the interrupt gate disables interrupts while it is
 *  running.
 *
 * @sa http://wiki.osdev.org/IDT
 */

#include <stdint.h>

#include "main/common.hpp"

namespace idt {
typedef void (*interrupt_handler_t)(idt_proc_state_t, addr_logical_t);
typedef void (*interrupt_handler_err_t)(idt_proc_state_t, uint32_t, addr_logical_t);

/** An IDT descriptor, as used by the `LIDT` instruction.
 */
struct __attribute__((packed)) descriptor_t {
  uint16_t size;  /**< The size of the IDT table in bytes, minus one */
  uinta_t offset; /**< The location of the IDT table in memory */
};

extern "C" volatile descriptor_t idt_descriptor;

/** An entry in the IDT */
struct entry_t {
  uint16_t offset_low;  /**< Bits 0..15 of the location of the handler */
  uint16_t selector;    /**< Segment selector */
  uint8_t zero;         /**< Must be zero */
  uint8_t type_attr;    /**< A set of flags followed by a gate type */
  uint16_t offset_high; /**< Bits 16..31 of the location of the handler */
#ifdef X86_64
  uint32_t offset_long;
  uint32_t zerob;
#endif
};

/** A type attr flag that if set, means the IDT entry is present */
const uint8_t FLAG_PRESENT = (1 << 7);
/** A type attr flag macro which sets which privilige level the calling discriptor must have */
#define IDT_FLAG_DPL(x) ((x) << 5)

/** 32 bit task gate */
const uint8_t GATE_32_TASK = 0x5;
/** 16 bit interrupt gate */
const uint8_t GATE_16_INT = 0x6;
/** 16 bit trap gate */
const uint8_t GATE_16_TRAP = 0x7;
/** 32 bit interrupt gate */
const uint8_t GATE_32_INT = 0xe;
/** 32 bit trap gate */
const uint8_t GATE_32_TRAP = 0xf;

/** Installs a handler into the system IDT.
 *
 * @ref init must have been called first.
 *
 * @param[in] id The ID of the interrupt to register for.
 * @param[in] offset The address of the function to jump to.
 * @param[in] selector The segment selector to use.
 * @param[in] type_attr The value to set as the type attr.
 */
void install(uint8_t id, interrupt_handler_t offset, uint16_t selector, uint8_t type_attr);

void install_with_error(uint8_t id, interrupt_handler_err_t offset, uint16_t selector, uint8_t type_attr);
/** Sets up and enables the system IDT.
 *
 * The created IDT will be enabled and loaded into the CPU (using LIDT), and all of its entries will be marked as "not
 *  present".
 */
void init();
void setup();

extern "C" {
#if X86_64
void idt_handle(uint32_t vector, idt_proc_state_t *state, addr_logical_t ret);
void idt_handle_with_error(uint32_t vector, uint32_t errcode, idt_proc_state_t *state, addr_logical_t ret);
#else
void idt_handle(uint32_t vector, idt_proc_state_t state, addr_logical_t ret);
void idt_handle_with_error(uint32_t vector, idt_proc_state_t state, uint32_t errcode, addr_logical_t ret);
#endif
}

extern "C" void idt_asm_interrupt_template();
extern "C" char idt_asm_interrupt_template_end;
extern "C" void idt_asm_interrupt_template_err();
extern "C" char idt_asm_interrupt_template_err_end;
} // namespace idt

#endif
