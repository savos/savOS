#pragma once

#include "main/multiboot.hpp"

extern "C" void populate_multiboot(multiboot::MultibootData *data, multiboot::info_t *mbi);
