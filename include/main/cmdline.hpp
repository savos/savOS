#pragma once

#include "main/common.hpp"

namespace cmdline {
void parse(char *line);

struct OptionPair {
  Utf8 name;
  Utf8 value;
};

Utf8 get_option_string(char *name, Utf8 def);

} // namespace cmdline
