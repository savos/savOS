#ifndef _HPP_MEM_OBJECT_
#define _HPP_MEM_OBJECT_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "mem/page.hpp"
#include "mem/vm.hpp"
#include "structures/list.hpp"
#include "structures/shared_ptr.hpp"

namespace object {
class PageEntry {
public:
  uint32_t offset;
  page::PageRaii page;
  unique_ptr<PageEntry> next;
};

struct PageAndOffset {
  page::PageRaii &page;
  size_t page_offset; // Offset in pages
};

class Object;

/** An Object is a generator that can generate pages when inserted into a specific virtual memory table
 *
 * Objects are inserted into `vm::Map`s, and are used to handle page faults for that memory map. For more details, see
 * documentation for that class.
 *
 * Types of object should inherit this base class, and provide a `do_generate` method in order to generate pages at a
 * given offset into this object.
 *
 * This object also contains a list of `ObjectInMap` instances which list all the maps it is used in. This should be
 * kept synchronised by the ObjectInMap itself. The actual addition of an object to a map is done automatically by the
 * map.
 */
class Object {
private:
  /** The list of Maps this object resides in.
   *
   * This is kept up to date by ObjectInMap itself.
   */
  list<ObjectInMap *> objects_in_maps;

public:
  unique_ptr<PageEntry> pages = nullptr;
  uint32_t max_pages;
  uint8_t page_flags;
  uint8_t object_flags;
  void *userdata = nullptr;

  Object(uint32_t max_pages, uint8_t page_flags, uint8_t object_flags, uint32_t offset);
  virtual ~Object();

  bool generate(uint32_t addr, uint32_t count);
  virtual page::PageRaii do_generate(addr_logical_t addr, uint32_t count) = 0;

  Failable<PageAndOffset> lookup_addr(addr_logical_t addr) const;

private:
  friend ObjectInMap;
  void add_object_in_map(ObjectInMap *oim);
  void remove_object_in_map(ObjectInMap *oim);
};

class EmptyObject : public Object {
public:
  EmptyObject(uint32_t max_pages, uint8_t page_flags, uint8_t object_flags, uint32_t offset)
      : Object(max_pages, page_flags, object_flags, offset){};
  page::PageRaii do_generate(addr_logical_t addr, uint32_t count) override;
};

class ObjectInMap {
public:
  shared_ptr<Object> object;
  vm::BaseMap *map;
  uint32_t base;
  int64_t offset;
  uint32_t pages;

  ObjectInMap(shared_ptr<Object> object, vm::BaseMap *map, uint32_t base, int64_t offset, uint32_t pages);
  ~ObjectInMap();
};
} // namespace object

#endif
