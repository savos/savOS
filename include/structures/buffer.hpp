#pragma once

#include <stddef.h>

#include "main/common.hpp"

/** A buffer represents a fixed length of pre-allocated memory
 *
 * A buffer contains a buffer and a number of elements, and supports access to the buffer using array subscripting. The
 * buffer will range check if NDEBUG is 0.
 *
 * Buffers may be released or populated. A released buffer has no contents, and any access will fail an assert.
 *
 * Buffers will not modify their pointer.
 *
 * A buffer will also fail an assert if it is destructed and its underlying buffer hasn't been released.
 */
template <class T> class Buffer {
private:
  T *contents;
  size_t elements;

public:
  /** Constructs a new released buffer */
  constexpr Buffer() : contents(nullptr), elements(0) {}
  /** Constructs a buffer over the provided pointer and size (in elements) */
  constexpr Buffer(T *buffer, size_t size) : contents(buffer), elements(size) {}
  /** Constructs a buffer over the provided pointer and size (in elements) */
  constexpr Buffer(void *buffer, size_t size) : contents(static_cast<T *>(buffer)), elements(size) {}
  constexpr Buffer(const Buffer<T> &other) = delete;
  /** Moves the provided buffer into this buffer
   *
   * `other` will be released after this.
   */
  constexpr Buffer(Buffer<T> &&other) : contents(other.contents), elements(other.elements) {
    other.contents = nullptr;
    other.elements = 0;
  }
  /** Moves the provided buffer into this buffer
   *
   * `other` will be released after this.
   */
  constexpr Buffer<T> &operator=(Buffer<T> &&other) {
    assert(!contents || "Buffer assigned to without releasing its contents!");
    contents = other.contents;
    elements = other.elements;
    other.contents = nullptr;
    other.elements = 0;
    return *this;
  }

  ~Buffer() { assert(!contents || "Buffer destroyed without releasing its contents!"); }

  /** Releases the contained pointer
   *
   * This puts the buffer in the released state, and returns the data it was previously holding. If it was already
   * released, this returns `nullptr`.
   */
  constexpr T *release() {
    T *old_contents = contents;
    contents = nullptr;
    elements = 0;
    return old_contents;
  }

  /** Returns a reference to the first element of the buffer */
  constexpr T &front() const { return contents[0]; }

  /** Returns a reference to the last element of the buffer */
  constexpr T &back() const { return contents[elements - 1]; }

  /** Returns true iff the buffer is released */
  constexpr bool released() const { return !contents; }

  /** Returns the size of the buffer in elements */
  constexpr size_t size() const { return elements; }

  /** Change the size of the underlying buffer in elements
   *
   * This only matters when doing range checking; the underlying pointer is not changed or resized.
   */
  constexpr void resize(size_t new_size) { elements = new_size; }

  /** Accesses the data at the given location */
  constexpr inline T &operator[](size_t pos) const {
    assert(pos < elements && "Out of bounds vector access!");
    assert(contents && "Vector is no longer available!");
    return contents[pos];
  }

  /** Returns an iterator to the first element of the buffer
   *
   * Just returns the pointer.
   */
  constexpr T *begin() const { return contents; }
  /** Returns a pointer one past the end of the buffer's contents */
  constexpr T *end() const volatile { return contents + elements; }
  /** Returns a constant iterator to the first element of the buffer
   *
   * Just returns the pointer.
   */
  constexpr const T *cbegin() const volatile { return contents; };
  /** Returns a constant pointer one past the end of the buffer's contents */
  constexpr const T *cend() const volatile { return contents + elements; }
};
