#pragma once

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include "main/common.hpp"

namespace elf {
const uint32_t EI_NIDENT = 16;

enum class SectionType : uint32_t {
  SHT_NULL = 0,
  PROGBITS = 1,
  SYMTAB = 2,
  STRTAB = 3,
  RELA = 4,
  HASH = 5,
  DYNAMIC = 6,
  NOTE = 7,
  NOBITS = 8,
  REL = 9,
  SHLIB = 10,
  DYNSYM = 11,
  LOPROC = 0x70000000,
  HIPROC = 0x7fffffff,
  LOUSER = 0x80000000,
  HIUSER = 0xffffffff
};

struct SectionHeader;
struct Symbol;
struct Header {
  uint8_t ident[EI_NIDENT];
  uint16_t type;
  uint16_t machine;
  uint32_t version;

  union {
    struct {
      uint32_t entry;
      uint32_t phoff;
      uint32_t shoff;
      uint32_t flags;
      uint16_t ehsize;
      uint16_t phentsize;
      uint16_t phnum;
      uint16_t shentsize;
      uint16_t shnum;
      uint16_t shstrndx;
    } width32;
    struct {
      uint64_t entry;
      uint64_t phoff;
      uint64_t shoff;
      uint32_t flags;
      uint16_t ehsize;
      uint16_t phentsize;
      uint16_t phnum;
      uint16_t shentsize;
      uint16_t shnum;
      uint16_t shstrndx;
    } width64;
  };

  bool is64Bit() const;

  size_t numSections() const;

  char *lookupString(uint32_t section, uint32_t offset) const;
  char *runtimeLookupString(uint32_t section, uint32_t offset) const;
  uint32_t sectionByType(SectionType type, uint32_t base) const;

  SectionHeader *sectionHeader(uint32_t id) const;
  void *sectionData(uint32_t id) const;
  void *runtimeSectionData(uint32_t id) const;
  uint32_t entriesInSection(uint32_t id) const;

  SectionHeader *sectionStringSection() const;
  void *sectionStringSectionData() const;
  void *runtimeSectionStringSectionData() const;
  char *lookupSectionString(uint32_t offset) const;
  char *runtimeLookupSectionString(uint32_t offset) const;

  Symbol *symbol(uint32_t section, uint32_t offset) const;
  Symbol *runtimeSymbol(uint32_t section, uint32_t offset) const;
  char *symbolName(uint32_t section, uint32_t offset) const;
  char *runtimeSymbolName(uint32_t section, uint32_t offset) const;

  uint32_t runtimeFindSymbolId(uint32_t addr, uint32_t type) const;
  Symbol *runtimeFindSymbol(uint32_t addr, uint32_t type) const;
  char *runtimeFindSymbolName(uint32_t addr, uint32_t type) const;
};

const uint8_t EI_MAG0 = 0;
const uint8_t EI_MAG1 = 1;
const uint8_t EI_MAG2 = 2;
const uint8_t EI_MAG3 = 3;
const uint8_t EI_CLASS = 4;
const uint8_t EI_DATA = 5;
const uint8_t EI_VERSION = 6;
const uint8_t EI_PAD = 7;

const uint8_t ELFMAG0 = 0x7f;
const uint8_t ELFMAG1 = 'E';
const uint8_t ELFMAG2 = 'L';
const uint8_t ELFMAG3 = 'F';
const uint8_t ELFCLASSNONE = 0;
const uint8_t ELFCLASS32 = 1;
const uint8_t ELFCLASS64 = 2;
const uint8_t ELFDATANONE = 0;
const uint8_t ELFDATA2LSB = 1;
const uint8_t ELFDATA2MSB = 2;


const uint16_t ET_NONE = 0;
const uint16_t ET_REL = 1;
const uint16_t ET_EXEC = 2;
const uint16_t ET_DYN = 3;
const uint16_t ET_CORE = 4;
const uint16_t ET_LOPROC = 0xff00;
const uint16_t ET_HIPROC = 0xffff;

const uint16_t EM_NONE = 0;
const uint16_t EM_M32 = 1;
const uint16_t EM_SPARC = 2;
const uint16_t EM_386 = 3;
const uint16_t EM_68K = 4;
const uint16_t EM_88K = 5;
const uint16_t EM_860 = 7;
const uint16_t EM_MIPS = 8;

const uint32_t EV_NONE = 0;
const uint32_t EV_CURRENT = 1;

struct SectionHeader {
  uint32_t name;
  SectionType type;

  union {
    struct {
      uint32_t flags;
      uint32_t addr;
      uint32_t offset;
      uint32_t size;
      uint32_t link;
      uint32_t info;
      uint32_t addralign;
      uint32_t entsize;
    } width32;
    struct {
      uint64_t flags;
      uint64_t addr;
      uint64_t offset;
      uint64_t size;
      uint32_t link;
      uint32_t info;
      uint64_t addralign;
      uint64_t entsize;
    } width64;
  };
};

const uint32_t SHN_UNDEF = 0;
const uint32_t SHN_LORESERVE = 0xff00;
const uint32_t SHN_LOPROC = 0xff00;
const uint32_t SHN_HIPROC = 0xff1f;
const uint32_t SHN_ABS = 0xfff1;
const uint32_t SHN_COMMON = 0xfff2;
const uint32_t SHN_HIRESERVE = 0xffff;

const uint32_t SHF_WRITE = 1;
const uint32_t SHF_ALLOC = 2;
const uint32_t SHF_EXECINSTR = 4;
const uint32_t SHF_MASKPROC = 0xf0000000;

struct Symbol {
  uint32_t name;
  uint32_t value;
  uint32_t size;
  uint8_t info;
  uint8_t other;
  uint16_t shndx;
};

uint8_t st_bind(uint8_t x);
uint8_t st_type(uint8_t x);
uint8_t st_info(uint8_t b, uint8_t t);

const uint32_t STB_LOCAL = 0;
const uint32_t STB_GLOBAL = 1;
const uint32_t STB_WEAK = 2;
const uint32_t STB_LOPROC = 13;
const uint32_t STB_HIPROC = 15;

const uint32_t STT_NOTYPE = 0;
const uint32_t STT_OBJECT = 1;
const uint32_t STT_FUNC = 2;
const uint32_t STT_SECTION = 3;
const uint32_t STT_FILE = 4;
const uint32_t STT_LOPROC = 13;
const uint32_t STT_HIPROC = 15;
} // namespace elf
