#ifndef _HPP_STRUCT_LIST_
#define _HPP_STRUCT_LIST_

#include <stddef.h>

#include "main/common.hpp"
#include "main/cpp.hpp"
#include "structures/iterator.hpp"
#include "structures/unique_ptr.hpp"

namespace list_ns {
/** Implements a linked list over a specified type, allowing dynamically sized storage
 *
 * This functions similarly to std::list, except it doesn't support custom deleters. Elements can be appended to either
 *  the front of the back of the list, and iteraters can traverse from the front to the back.
 *
 * This is implemented as a singly-linked list, so appending an element to the back of this list will run in `O(n)`
 *  time. Besides this and list::clear (which also runs in `O(n)` time), all operations run in `O(1)` time.
 *
 * Adding or removing elements is not thread safe.
 *
 * When an object is removed from the list, their default deconstructor is called, and any references/pointers to them
 *  are invalid. Deleting an element does not invalidate any references/pointers to other elements or the list itself.
 *
 * @TODO This needs to be thread safe
 */
template <class T> class list {
private:
  struct Entry {
    unique_ptr<Entry> next;
    T object;

    constexpr Entry(const T &obj) : next(nullptr), object(obj){};
    constexpr Entry(T &&obj) : next(nullptr), object(move(obj)){};
  };

  unique_ptr<Entry> head = nullptr;
  Entry *tail = nullptr;
  size_t count = 0;

public:
  /** An iterator for lists
   *
   * See list::begin for more details.
   */
  class Iterator {
    friend list;

  private:
    Entry *entry;

  public:
    constexpr Iterator(Entry *e) : entry(e) {}
    constexpr Iterator(const Iterator &e) : entry(e.entry) {}

    constexpr T &operator*() const { return entry->object; }
    constexpr T *operator->() const { return &(entry->object); }
    constexpr Iterator &operator++() {
      entry = entry->next.get();
      return *this;
    }
    constexpr Iterator operator++(int) {
      Iterator result(*this);
      ++(*this);
      return result;
    }
    constexpr bool operator==(const Iterator &other) const { return other.entry == entry; }
    constexpr bool operator!=(const Iterator &other) const { return other.entry != entry; }
  };

  class CIterator {
    friend list;

  private:
    const Entry *entry;

  public:
    constexpr CIterator(const Entry *e) : entry(e) {}
    constexpr CIterator(const CIterator &e) : entry(e.entry) {}

    constexpr const T &operator*() const { return entry->object; }
    constexpr const T *operator->() const { return &(entry->object); }
    constexpr CIterator &operator++() {
      entry = entry->next.get();
      return *this;
    }
    constexpr CIterator operator++(int) {
      CIterator result(*this);
      ++(*this);
      return result;
    }
    constexpr bool operator==(const CIterator &other) const { return other.entry == entry; }
    constexpr bool operator!=(const CIterator &other) const { return other.entry != entry; }
  };

  /** Constructs a new, empty list */
  constexpr list(){};
  /** Constructs a list of `count` default-constructed elements
   *
   * @param count The number of default constructed elements to create
   */
  constexpr list(size_t count) {
    while (count--) {
      emplace_front();
    }
  }

  /** Returns a reference to the frontmost element of the list
   *
   * @return The front element
   */
  constexpr T &front() const { return head->object; }

  /** Returns a reference to the backmost element of the list
   *
   * @return The back element
   */
  constexpr T &back() const { return tail->object; }

  /** Returns true iff the list has no elements
   *
   * @return Whether the list is empty
   */
  constexpr bool empty() const {
    assert(!(!head && tail) && "List is in an inconsistent state");
    return !head;
  }

  /** Returns the number of items in the list
   *
   * @return How many items are in this list
   */
  constexpr size_t size() const { return count; }

  /** Deletes all items in the list
   *
   * After this call, the list will be empty.
   */
  constexpr void clear() {
    head = nullptr;
    tail = nullptr;
    count = 0;
  }

  /** Push an item to the front of the list
   *
   * The element will be copied.
   *
   * @param value The item to add
   */
  void push_front(const T &value) {
    unique_ptr<Entry> append = make_unique<Entry>(value);

    if (head) {
      append->next = move(head);
      head = move(append);
    } else {
      head = move(append);
      tail = head.get();
    }
    count++;
  }
  /** Push an item to the front of the list
   *
   * The element will be moved.
   *
   * @param value The item to add
   */
  void push_front(T &&value) {
    unique_ptr<Entry> append = make_unique<Entry>(move(value));

    if (head) {
      append->next = move(head);
      head = move(append);
    } else {
      head = move(append);
      tail = head.get();
    }
    count++;
  }
  /** Remove the front item from the list
   *
   * The item will be deleted, and the new front will be the item following it.
   */
  void pop_front() {
    assert(head);

    unique_ptr<Entry> old_head = move(head);
    head = move(old_head->next);

    if (!head) {
      tail = nullptr;
    }

    count--;
  }
  /** Construct a new item at the front of the list
   *
   * @param args The arguments for the newly created item's constructor
   */
  template <class... Args> void emplace_front(Args &&... args) {
    unique_ptr<Entry> append = make_unique<Entry>(T(forward<Args>(args)...));

    if (head) {
      append->next = move(head);
      head = move(append);
    } else {
      head = move(append);
      tail = head.get();
    }
    count++;
  }

  /** Push an item to the back of the list
   *
   * The element will be copied.
   *
   * @param value The item to add
   */
  constexpr void push_back(const T &value) {
    unique_ptr<Entry> append = make_unique<Entry>(value);

    if (head) {
      tail->next = move(append);
      tail = tail->next.get();
    } else {
      head = move(append);
      tail = head.get();
    }
    count++;
  }
  /** Push an item to the back of the list
   *
   * The element will be moved.
   *
   * @param value The item to add
   */
  void push_back(T &&value) {
    unique_ptr<Entry> append = make_unique<Entry>(value);

    if (head) {
      tail->next = move(append);
      tail = tail->next.get();
    } else {
      head = move(append);
      tail = head.get();
    }
    count++;
  }
  /** Remove the back item from the list
   *
   * The item will be deleted, and the new back will be the item preceding it.
   */
  void pop_back() {
    assert(head);

    if (head.get() == tail) {
      head = nullptr;
      tail = nullptr;
      count--;
      return;
    }

    Entry *e;
    for (e = head.get(); e->next.get() != tail; e = e->next.get()) {
    }

    tail = e;
    e->next = nullptr;

    count--;
  }
  /** Construct a new item at the back of the list
   *
   * @param args The arguments for the newly created item's constructor
   */
  template <class... Args> void emplace_back(Args &&... args) {
    unique_ptr<Entry> append = make_unique<Entry>(T(forward<Args>(args)...));

    if (head) {
      tail->next = move(append);
      tail = tail->next.get();
    } else {
      head = move(append);
      tail = head.get();
    }

    count++;
  }

  /** Removes all elements equal to the provided value
   *
   * @param value The value to remove
   */
  void remove(const T &value) {
    if (!head)
      return;

    Entry *prev = nullptr;
    Entry *now = head.get();
    bool skip;

    for (; now; skip || ((prev = now), (now = now->next.get()))) {
      skip = false;
      if (now->object == value) {
        if (prev) {
          prev->next = move(now->next);
          now = prev->next.get();
        } else {
          head = move(now->next);
          now = head.get();
        }
        count--;
        skip = true;
      }
    }

    tail = prev;
  }
  /** Removes the element that the iterator is pointing to
   *
   * @param it The iterator to remove the element at
   * @return A new iterator, pointing to the element following the deleted one
   */
  Iterator erase(Iterator pos) {
    Entry *entry = pos.entry;
    pos++;

    Entry *prev = nullptr;
    Entry *now = head.get();

    for (; now; (prev = now), (now = now->next.get())) {
      if (now == entry) {
        if (!now->next) {
          tail = prev;
        }
        if (prev) {
          prev->next = move(now->next);
        } else {
          head = move(now->next);
        }
        count--;
        return pos;
      }
    }

    panic("Tried to erase an iterator that isn't in the list");
  }

  /** Returns an iterator to the first element of the list
   *
   * This iterator will start at the front of the list, and advance through all the items in order.
   *
   * Modifying an item is allowed, as is appending a new item (whether this item gets iterated through is undefined).
   *  Removing an item other than the current item is also allowed, and it will not be iterated through if it has not
   *  been seen yet. Removing the current item is not allowed.
   *
   * @return An iterater through the list's contents
   */
  constexpr Iterator begin() { return Iterator(head.get()); }
  /** Returns a sentinel value that represents the end of an iteration
   *
   * Do not attempt to use this as an iterated value.
   *
   * @return A null iterator
   */
  constexpr Iterator end() { return Iterator(nullptr); }
  /** Returns a constant iterator to the first element of the list
   *
   * This iterator will start at the front of the list, and advance through all the items in order.
   *
   * @return An iterater through the list's contents
   */
  constexpr CIterator cbegin() const { return CIterator(head.get()); }
  /** Returns a sentinel value that represents the end of a constant iteration
   *
   * Do not attempt to use this as an iterated value.
   *
   * @return A null iterator
   */
  constexpr CIterator cend() const { return CIterator(nullptr); }
};
} // namespace list_ns
#endif
