#ifndef _H_STRUCTURES_MUTEX_
#define _H_STRUCTURES_MUTEX_


#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include "main/asm_utils.hpp"
#include "main/errno.h"

/** Contains a thread mutex
 */
namespace mutex {
/** Implementation of a mutex, supporting lock, trylock and unlock.
 *
 * These mutexes must only be used in a thread.
 */
class Mutex {
public:
  /** Gets a lock on the mutex, or blocks
   *
   * If the mutex is already locked, this blocks until the lock is released.
   *
   * TODO: This is currently implemented as a while(true) loop, it should be changed to thread yielding.
   *
   * @return EOK when we get the lock
   */
  int lock();
  /** Gets a lock on the mutex, or returns EBUSY
   *
   * @return EOK when the mutex is locked or EBUSY if is already locked
   */
  int trylock();
  /** Unlock a previously locked mutex
   *
   * @return EOK
   */
  int unlock();

private:
  volatile bool flag = false;
};

class LockGuard {
  Mutex &mtx;

public:
  LockGuard(Mutex &mtx) : mtx(mtx) { mtx.lock(); }

  ~LockGuard() { mtx.unlock(); }
};

class CliLockGuard {
  Mutex &mtx;
  uint32_t flags;

public:
  CliLockGuard(Mutex &mtx) : mtx(mtx) {
    flags = push_cli();
    mtx.lock();
  }

  ~CliLockGuard() {
    mtx.unlock();
    pop_flags(flags);
  }
};
} // namespace mutex

#endif
