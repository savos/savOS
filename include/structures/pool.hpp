#pragma once

#include <stddef.h>

#include "debug/assert.hpp"
#include "main/cpp.hpp"

namespace {
#if CHECK_POOL_INIT
const uint32_t _SENTINEL = 0xdeadbeef;
#endif
} // namespace

namespace pool {
template <class T> class Pool {
  struct Entry {
    T *object;
    Entry *next;
  };

  Entry *head;
  Entry *empty;

#if CHECK_POOL_INIT
  uint32_t sentinel_store;
#endif

public:
  constexpr Pool()
      : head(nullptr),
        empty(nullptr)
#if CHECK_POOL_INIT
        ,
        sentinel_store(_SENTINEL)
#endif
            {};

  T *alloc(uint8_t alloc_flag = 0) {
#if CHECK_POOL_INIT
    assert(sentinel_store == _SENTINEL);
#endif

    if (head) {
      Entry *old_head = head;

      head = head->next;

      old_head->next = empty;
      empty = old_head;
      return old_head->object;
    } else {
      if (!alloc_flag) {
        return new T();
      } else {
        return reinterpret_cast<T *>(kmem::kmalloc(sizeof(T), alloc_flag));
      }
    }
  }

  void free(T *object, uint8_t alloc_flag = 0) {
#if CHECK_POOL_INIT
    assert(sentinel_store == _SENTINEL);
#endif

    if (empty) {
      empty->object = object;
      empty->next = head;
      head = empty;
      empty = head->next;
    } else {
      Entry *created;
      if (!alloc_flag) {
        created = new Entry();
      } else {
        created = reinterpret_cast<Entry *>(kmem::kmalloc(sizeof(Entry), alloc_flag));
      }
      created->object = object;
      created->next = head;
      head = created;
    }
  }

  constexpr bool has_item() const { return head != nullptr; }
};
} // namespace pool
