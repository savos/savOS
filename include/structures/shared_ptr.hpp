#ifndef _HPP_STRUCT_SHARED_PTR_
#define _HPP_STRUCT_SHARED_PTR_

#include <stddef.h>
#include <stdint.h>

#include "debug/assert.hpp"
#include "main/cpp.hpp"

namespace shared_ptr_ns {
struct Data {
  uint32_t uses = 1;
};

/** A smart pointer where multiple own and manage a single object, deleting it when all shared_ptrs goes out of scope
 *
 * Multiple shared_ptrs can own the same object, and the object is deleted only when there are no more shared_ptrs
 *  owning it (e.g., when the last one is deleted or falls out of scope).
 *
 * This is an implementation of shared_ptr from C++11, with the following differences:
 * * A custom deleter is not yet supported.
 * * Pointer comparsions are not yet supported.
 */
template <class T> class shared_ptr {
public:
  /** Create a new empty shared_ptr
   */
  constexpr shared_ptr() : ref(nullptr), data(nullptr){};
  /** Create a new shared_ptr owning `ref`
   *
   * @param ref The pointer to own
   */
  shared_ptr(T *ref) : ref(ref) {
    if (ref) {
      data = new Data();
    }
  };
  /** Create a new shared_ptr managing the shared resource from the given shared_ptr
   *
   * @param other The other shared_ptr to share from
   */
  template <class U> constexpr shared_ptr(const shared_ptr<U> &other) : ref(other.ref) {
    data = other.data;
    if (ref) {
      data->uses++;
    }
  }
  /** Create a new shared_ptr from the given shared_ptr
   *
   * After construction, the other pointer will be empty.
   *
   * @param other The other shared_ptr to steal from
   */
  template <class U> constexpr shared_ptr(shared_ptr<U> &&other) : ref(other.ref), data(other.data) {
    other.ref = nullptr;
    other.data = nullptr;
  }
  /** Create a new shared_ptr managing the shared resource from the given shared_ptr
   *
   * @param other The other shared_ptr to share from
   */
  constexpr shared_ptr(const shared_ptr &other) : ref(other.ref), data(other.data) {
    if (ref) {
      data->uses++;
    }
  }
  /** Create a new shared_ptr from the given shared_ptr
   *
   * After construction, the other pointer will be empty.
   *
   * @param other The other shared_ptr to steal from
   */
  constexpr shared_ptr(shared_ptr &&other) : ref(other.ref), data(other.data) {
    other.ref = nullptr;
    other.data = nullptr;
  }

  /** Deletes the associated object, if no other shared_ptrs own it
   */
  ~shared_ptr() { decrement_usage(); }

  /** Get the object (if any) from the other shared_ptr, and share it with this shared_ptr
   *
   * Any existing object owned by this shared_ptr will be deleted if this is the last shared_ptr owning it.
   */
  shared_ptr &operator=(shared_ptr &r) {
    if (r.ref) {
      r.data->uses++;
    }
    decrement_usage();

    ref = r.ref;
    data = r.data;

    return *this;
  }
  /** Copy the object (if any) from the other shared_ptr
   *
   * The other shared_ptr will be empty after this call.
   *
   * Any existing object owned by this shared_ptr will be deleted if this is the last shared_ptr owning it.
   */
  shared_ptr &operator=(shared_ptr &&r) {
    decrement_usage();
    ref = r.ref;
    r.ref = nullptr;
    data = r.data;
    r.data = nullptr;

    return *this;
  }
  /** Clears the object managed by this shared_ptr.
   *
   * It will be deleted if no other shared_ptrs own it.
   */
  shared_ptr &operator=(cpp::nullptr_t r) {
    decrement_usage();
    ref = nullptr;
    data = nullptr;

    return *this;
  }
  /** Compare two shared_ptrs for equality
   *
   * Two shared_ptrs are equal if they own the same object
   */
  constexpr bool operator==(const shared_ptr &r) { return r.ref == ref; }
  /** Compare two shared_ptrs for inequality
   *
   * Two shared_ptrs are not equal if they own different objects
   */
  constexpr bool operator!=(const shared_ptr &r) { return r.ref != ref; }

  /** Returns true iff this shared_ptr is not empty
   */
  constexpr operator bool() const { return ref != nullptr; }

  /** Changes the object managed by this shared_ptr
   *
   * Any existing managed object will be deleted if this is the last shared_ptr owning it.
   *
   * @param ptr The new object to set
   */
  void reset(T *ptr) {
    decrement_usage();

    ref = ptr;
    data = new Data();
  }
  /** Swaps managed objects with the provided shared_ptr
   *
   * @param other The shared_ptr to swap with.
   */
  constexpr void swap(shared_ptr<T> &other) {
    T *otherRef = other.ref;
    other.ref = ref;
    ref = otherRef;

    Data *otherData = other.data;
    other.data = data;
    data = otherData;
  }
  /** Returns the number of shared_ptrs sharing the resource */
  constexpr uint32_t use_count() { return data->uses; }

  /** Returns the managed object */
  constexpr T *get() const { return ref; }

  /** Dereferences the managed object */
  constexpr typename cpp::add_lvalue_reference<T>::type operator*() const {
    assert(ref && "shared_ptr is null");
    return *get();
  }

  /** Returns a reference to element idx in an array */
  constexpr T &operator[](ptrdiff_t idx) const {
    assert(ref && "shared_ptr is null");
    return *(ref + idx);
  }

  /** Dereferences the managed object */
  constexpr T *operator->() const {
    assert(ref && "shared_ptr is null");
    return get();
  }

private:
  template <class U> friend class shared_ptr;

  T *ref;
  Data *data;

  // Also deletes if appropriate
  void decrement_usage() {
    if (ref) {
      data->uses--;
      if (!data->uses) {
        delete ref;
        delete data;
      }
    }
  } // Also deletes if appropriate
};

/** Creates a shared_ptr managing a newly created and allocated T
 *
 * The constructor for T will be called as appropriate depending on `args`.
 *
 * @param args The arguments to pass through to T's constructor.
 * @return A shared_ptr to the newly allocated object.
 */
template <class T, class... Args> shared_ptr<T> make_shared(Args &&... args) {
  return shared_ptr<T>(new T(cpp::forward<Args>(args)...));
}
} // namespace shared_ptr_ns

#endif
