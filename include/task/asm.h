#ifndef _H_TASK_ASM_
#define _H_TASK_ASM_

#include <stddef.h>

void task_asm_entry_point();
#if X86_64
void __attribute__((noreturn)) task_asm_enter(uintptr_t sp);
int task_asm_yield(uintptr_t sp);
int task_asm_set_stack(uintptr_t sp, void (*next)());
#else
void __attribute__((fastcall, noreturn)) task_asm_enter(uintptr_t sp);
int __attribute__((fastcall)) task_asm_yield(uintptr_t sp);
int __attribute__((fastcall)) task_asm_set_stack(uintptr_t sp, void (*next)());
#endif

#endif
