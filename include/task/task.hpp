#ifndef _HPP_TASK_TASK_
#define _HPP_TASK_TASK_

#include <stddef.h>

#include "mem/object.hpp"
#include "mem/page.hpp"
#include "mem/vm.hpp"
#include "structures/list.hpp"
#include "structures/shared_ptr.hpp"
#include "structures/unique_ptr.hpp"

namespace task {
const uint32_t TASK_STACK_TOP = KERNEL_VM_BASE;
const uint64_t TASK_STACK_TOP_64 = KERNEL_VM_BASE_64;

class Process;
class Thread;

typedef uint32_t wchan_t;

extern bool inited;

class Process {
private:
  list<shared_ptr<Thread>> threads;

public:
  uint32_t process_id;
  uint32_t owner;
  uint32_t group;
  uint32_t thread_counter;

  Process(uint32_t owner, uint32_t group);
  shared_ptr<Thread> new_thread(addr_logical_t entry_point);
  shared_ptr<Thread> get_thread(uint32_t id) const;
  void remove_thread(uint32_t id);
};

enum class ThreadState : uint8_t {
  /** Thread is newly created, and about to to be added to the threads pending list
   *
   * Goes into the `pending` state when inserted into the list.
   */
  created,
  /** Thread is currently executing on a core
   *
   * Goes to `prepending` when it calls `thread_yield`.
   * Goes to `yielding` when it calls `wait`.
   * Goes to `ended` when it calls `end`.
   * Goes to `runcontinue` when `resume_if` is called on it.
   *
   */
  running,
  /** Thread has received a resume while it was running
   *
   * Goes to `running` when it calls `wait` (instead of yielding).
   * Otherwise behaves the same as `running`.
   */
  runcontinue,
  /** Thread is currently in the process of exiting due to a `wait` call
   *
   * Normally goes to `waiting`, but if the thread receives a resume, goes to `prepending` instead.
   */
  yielding,
  /** Thread is waiting for a `resume` to be called on it
   *
   * When this happens, it goes into the `pending` state.
   */
  waiting,
  /** Thread is currently yielding, and will go into the `pending` threads list when done
   *
   * Will go into the `pending` state after this.
   */
  prepending,
  /** Thread is currently in the list of threads waiting to be executed
   *
   * When selected and in the process of loading, it goes into the `running` state.
   */
  pending,
  /** Thread has called `task_end` and is no longer running.
   *
   * It will not leave this state.
   */
  ended,
};

class Thread {
public:
  shared_ptr<Process> process;

  uint32_t thread_id;
  uint32_t task_id;

  unique_ptr<vm::BaseMap> vm;

  shared_ptr<object::Object> stack;
  addr_logical_t stack_pointer;

  /** Waiting channel
   *
   * If set to anything other than `no_wchan`, this thread is waiting for that signal. This is guarded by this
   * thread's `state_mutex`.
   */
  wchan_t wchan;
  /** Pending waiting channel
   *
   * Used for when a resume happens before a wait. If a wait happens with this wchan, then the wait is skipped.
   * This is guarded by this thread's `state_mutex`.
   */
  wchan_t pending_wchan;
  /** Mutex for guarding this thread's state from multiple threads. */
  Mutex state_mutex;

  ThreadState state;

  Thread(shared_ptr<Process> process, addr_logical_t entry);
  ~Thread();

  void end();
};

extern shared_ptr<Process> kernel_process;
shared_ptr<Process> get_process(uint32_t id);

void init();

void task_yield();
extern "C" void __attribute__((noreturn)) task_enter(shared_ptr<Thread> thread);
extern "C" void __attribute__((noreturn)) task_yield_done(uint32_t sp);
extern "C" void task_timer_yield();
extern "C" void __attribute__((noreturn)) task_end();
void __attribute__((noreturn)) schedule();
void wait(wchan_t wchan);

wchan_t new_wchan(Utf8 name);

bool in_thread();
shared_ptr<Thread> get_thread();

void resume_if(shared_ptr<Thread> thread, wchan_t whchan);
} // namespace task

#endif
