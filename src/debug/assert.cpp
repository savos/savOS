#include "debug/assert.hpp"

#if NDEBUG
#else
namespace assert {
void do_assert(const bool condition, const char *expr, const int lineno, const char *function, const char *file) {
  if (!condition) {
    panic("Assertion %s failed\nIn %s in %s:%i", expr, function, file, lineno);
  }
}
} // namespace assert
#endif
