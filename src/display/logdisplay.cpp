#include "display/logdisplay.hpp"

#include "logging/log.hpp"
#include "main/vga.hpp"
#include "utils/text.hpp"

namespace log_display {

namespace {
const char *TITLE = " SavOS - Kernel Log";
vector<vector<size_t>> log_breaks;

size_t update_log_breaks(int width) {
  size_t entries = log::entries.size();
  //log_breaks.reserve(entries);

  for (size_t i = log_breaks.size(); i < entries; i++) {
    log_breaks.push_back(utils_text::ascii_break_into_lines(log::entries[i].message, width));
  }

  return entries;
}
} // namespace


LogDisplay::LogDisplay(Buffer<volatile uint16_t> &buffer, size_t id) : Display(buffer, id) {}
void LogDisplay::enable() {
  Display::enable();
  update();
}

void LogDisplay::update() {
  if (!active)
    return;

  memset_v(buffer.begin(), 0, buffer.size());

  // Header bar
  for (int x = 0; x < cols; x++) {
    if (x > static_cast<int>(strlen(TITLE))) {
      buffer[x] = vga::add_colour('\0', vga::entry_colour(vga::COLOUR_WHITE, vga::COLOUR_BLUE));
    } else {
      buffer[x] = vga::add_colour(TITLE[x], vga::entry_colour(vga::COLOUR_WHITE, vga::COLOUR_BLUE));
    }
  }

  int width = cols - 2;

  size_t log_entries = update_log_breaks(width);

  signed int start_line = rows - 1;

  // Each entry
  for (int i = log_entries - 1; i >= 0; i--) {
    if (start_line < 1) {
      break;
    }
    // Need to copy here, in case the log gets reallocated
    auto entry = log::entries[i];
    auto &breaks = log_breaks[i];

    start_line -= breaks.size() + 1;
    signed int print_line = start_line;
    size_t print_col = 1;
    size_t break_p = 0;

    auto gutter = [&](char symbol) {
      if (print_line >= 1) {
        buffer[print_line * cols] = vga::add_colour(symbol, vga::entry_colour(vga::COLOUR_DARK_GREY, vga::COLOUR_BLUE));
        buffer[print_line * cols + width + 1] =
            vga::add_colour(' ', vga::entry_colour(vga::COLOUR_WHITE, vga::COLOUR_BLUE));
      }
    };

    gutter(' ');
    for (uint32_t c = 0; c < entry.message.bytes(); c++) {
      if (break_p < breaks.size() && c == breaks[break_p]) {
        break_p++;
        print_col = 1;
        print_line++;
        gutter('>');
      }

      if (print_line >= 1) {
        buffer[print_line * cols + print_col] =
            vga::add_colour(entry.message[c], vga::entry_colour(vga::COLOUR_WHITE, vga::COLOUR_BLACK));
      }

      print_col++;
    }
  }

  // Footer bar
  for (int x = 0; x < cols; x++) {
    buffer[(rows - 1) * cols + x] = vga::add_colour('\0', vga::entry_colour(vga::COLOUR_WHITE, vga::COLOUR_BLUE));
  }
}
} // namespace display
