#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "hw/acpi.hpp"
#include "hw/acpica.hpp"
#include "hw/pci/pci.hpp"
#include "main/common.hpp"

namespace {
ACPI_STATUS get_irq_resource(ACPI_RESOURCE *res, void *context) {
  uint32_t *source_index = reinterpret_cast<uint32_t *>(context);
  if (res->Type == ACPI_RESOURCE_TYPE_IRQ) {
    panic("[PCI] Regular IRQs are not supported.");
  } else if (res->Type == ACPI_RESOURCE_TYPE_EXTENDED_IRQ) {
    uint32_t irq = res->Data.ExtendedIrq.Interrupts[*source_index];
    pci::tell_irq(irq);
  }

  return AE_OK;
}

ACPI_STATUS dev_get_routing(ACPI_HANDLE handle, UINT32 level, void *context, void **retval) {
  ACPI_BUFFER pci_table_buff;
  pci_table_buff.Length = ACPI_ALLOCATE_BUFFER;
  pci_table_buff.Pointer = nullptr;
  ACPI_STATUS error;

  auto check = [&](const char *message) {
    if (ACPI_FAILURE(error)) {
      panic("ACPI get routing error (%d): %s", error, message);
    }
  };

  error = AcpiGetIrqRoutingTable(handle, &pci_table_buff);
  if (ACPI_FAILURE(error)) {
    return AE_OK;
  }

  auto pci_table = reinterpret_cast<ACPI_PCI_ROUTING_TABLE *>(pci_table_buff.Pointer);
  while (pci_table->Length != 0) {
    if (*(char *)pci_table->Source == '\0') {
      // Interrupt is just SourceIndex, not a resource
      pci::tell_irq(pci_table->SourceIndex);
    } else {
      ACPI_HANDLE src_handle;
      error = AcpiGetHandle(handle, pci_table->Source, &src_handle);
      check("AcpiGetHandle");

      error = AcpiWalkResources(src_handle, METHOD_NAME__CRS, get_irq_resource, &pci_table->SourceIndex);
      check("AcpiWalkResources");
    }

    pci_table = reinterpret_cast<ACPI_PCI_ROUTING_TABLE *>(reinterpret_cast<char *>(pci_table) + pci_table->Length);
  }

  return AE_OK;
}

ACPI_STATUS root_dev_get_routing(ACPI_HANDLE handle, UINT32 level, void *context, void **retval) {
  return AcpiGetDevices(NULL, dev_get_routing, NULL, NULL);
}
} // namespace

namespace acpi {
vector<MadtProc> procs;
vector<MadtIoapic> ioapics;
vector<MadtIso> isos;

uint32_t proc_count;
uint32_t ioapic_count;
uint32_t iso_count;

addr_phys_t lapic_base;
bool acpi_found;
bool inited = false;

void init() {
  ACPI_STATUS error;

  auto check = [&](const char *message) {
    if (ACPI_FAILURE(error)) {
      panic("ACPI Initialization error (%d): %s", error, message);
    }
  };

  error = AcpiInitializeSubsystem();
  check("AcpiInitializeSubsystem");
  error = AcpiInitializeTables(NULL, 16, FALSE);
  check("AcpiInitializeTables");
  error = AcpiLoadTables();
  check("AcpiLoad");
  error = AcpiEnableSubsystem(ACPI_FULL_INITIALIZATION);
  check("AcpiEnableSubystem");
  error = AcpiInitializeObjects(ACPI_FULL_INITIALIZATION);
  check("AcpiInitializeObjects");

  ACPI_TABLE_HEADER *header;
  error = AcpiGetTable(ACPI_SIG_MADT, 1, &header);
  check("AcpiGetTable");
  ACPI_TABLE_MADT *madt = (ACPI_TABLE_MADT *)header;

  lapic_base = madt->Address;

  // Load the table information
  uint32_t offset = sizeof(ACPI_TABLE_MADT);
  ACPI_SUBTABLE_HEADER *subtable = ACPI_ADD_PTR(ACPI_SUBTABLE_HEADER, madt, offset);
  while (offset < header->Length) {
    switch (subtable->Type) {
    case ACPI_MADT_TYPE_LOCAL_APIC: {
      ACPI_MADT_LOCAL_APIC *local_apic = reinterpret_cast<ACPI_MADT_LOCAL_APIC *>(subtable);
      procs.emplace_back();
      procs.back().id = local_apic->Id;
      procs.back().apic_id = local_apic->ProcessorId;
      procs.back().flags = local_apic->LapicFlags;
      proc_count++;
      break;
    }

    case ACPI_MADT_TYPE_IO_APIC:
      // InfoTable = AcpiDmTableInfoMadt1;
      break;

    case ACPI_MADT_TYPE_INTERRUPT_OVERRIDE: {
      auto *override = reinterpret_cast<ACPI_MADT_INTERRUPT_OVERRIDE *>(subtable);
      isos.emplace_back();
      isos.back().bus_source = override->Bus;
      isos.back().irq_source = override->SourceIrq;
      isos.back().global_int = override->GlobalIrq;
      isos.back().flags = override->IntiFlags;
      break;
    }

    default:
      (void)0; // Ignore it
    }

    /* Point to next subtable */

    offset += subtable->Length;
    subtable = ACPI_ADD_PTR(ACPI_SUBTABLE_HEADER, subtable, subtable->Length);
  }

  /* Set APIC Mode */
  ACPI_OBJECT arg1;
  ACPI_OBJECT_LIST args;
  arg1.Type = ACPI_TYPE_INTEGER;
  arg1.Integer.Value = 1; /* 0 - PIC, 1 - IOAPIC, 2 - IOSAPIC */
  args.Count = 1;
  args.Pointer = &arg1;

  AcpiEvaluateObject(ACPI_ROOT_OBJECT, "_PIC", &args, NULL);

  acpi_found = true;
  inited = true;
}

void init_pci() {
  // Get PCI routing tables
  AcpiGetDevices("PNP0A03", root_dev_get_routing, NULL, NULL);
}

uint32_t get_irq_override(uint8_t source) {
  for (const auto &iso : isos) {
    if (iso.irq_source == source) {
      return iso.global_int;
    }
  }
  return source;
}
} // namespace acpi
