#include "hw/acpica.hpp"
#include "hw/acpi.hpp"
#include "hw/cmos.hpp"
#include "hw/pci/pci.hpp"
#include "hw/utils.h"
#include "main/common.hpp"
#include "task/task.hpp"

extern "C" {

// Implementation of ACPICA's OS methods


void *AcpiOsAllocate(ACPI_SIZE Size) { return kmem::kmalloc(Size, 0); }

void AcpiOsFree(void *Memory) { return kmem::kfree(Memory); }

ACPI_THREAD_ID AcpiOsGetThreadId() {
  if (!task::inited)
    return 0xff; // Placeholder value
  return task::get_thread()->task_id;
}

namespace {
struct Lock {
  Mutex mut;
  bool locked;
  uint32_t eflags;
};
} // namespace

ACPI_STATUS AcpiOsCreateLock(ACPI_SPINLOCK *OutHandle) {
  Lock *l = new Lock();
  *OutHandle = reinterpret_cast<ACPI_SPINLOCK>(l);
  return AE_OK;
}

ACPI_CPU_FLAGS AcpiOsAcquireLock(ACPI_SPINLOCK Handle) {
  uint32_t eflags = push_cli();
  auto l = reinterpret_cast<Lock *>(Handle);
  l->mut.lock();
  if (l->locked) {
    // Lock held
    while (l->locked) {
      // Spin!
      l->mut.unlock();
      l->mut.lock();
    }
    l->locked = true;
  } else {
    // Lock not held
    l->locked = true;
  }
  l->mut.unlock();
  return eflags;
}

void AcpiOsReleaseLock(ACPI_SPINLOCK Handle, ACPI_CPU_FLAGS Flags) {
  auto l = reinterpret_cast<Lock *>(Handle);
  l->mut.lock();
  l->locked = false;
  l->mut.unlock();
  pop_flags(Flags);
}

void AcpiOsDeleteLock(ACPI_SPINLOCK Handle) { delete reinterpret_cast<Lock *>(Handle); }

namespace {
struct Semaphore {
  Mutex mut;
  uint32_t units;
};
} // namespace

ACPI_STATUS AcpiOsCreateSemaphore(UINT32 MaxUnits, UINT32 InitialUnits, ACPI_SEMAPHORE *OutHandle) {
  (void)MaxUnits;
  Semaphore *s = new Semaphore();
  s->units = InitialUnits;
  *OutHandle = reinterpret_cast<ACPI_SEMAPHORE>(s);
  return AE_OK;
}

ACPI_STATUS AcpiOsWaitSemaphore(ACPI_SEMAPHORE Handle, UINT32 Units, UINT16 Timeout) {
  auto s = reinterpret_cast<Semaphore *>(Handle);
  s->mut.lock();
  if (s->units == 0) {
    // Lock held
    if (Timeout == 0) {
      s->mut.unlock();
      return AE_TIME;
    } else if (Timeout == 0xffff) {
      while (s->units) {
        s->mut.unlock();
        task::task_yield();
        s->mut.lock();
      }
      s->units = Units;
    } else {
      kerror("ACPICA: Non-zero timeout param not supported yet!\n");
    }
  } else {
    // Lock not held
    s->units -= Units;
  }
  s->mut.unlock();
  return AE_OK;
}

ACPI_STATUS AcpiOsSignalSemaphore(ACPI_SEMAPHORE Handle, UINT32 Units) {
  auto s = reinterpret_cast<Semaphore *>(Handle);
  s->mut.lock();
  s->units += Units;
  s->mut.unlock();
  return AE_OK;
}

ACPI_STATUS AcpiOsDeleteSemaphore(ACPI_SEMAPHORE Handle) {
  delete reinterpret_cast<Semaphore *>(Handle);
  return AE_OK;
}

UINT64 AcpiOsGetTimer() { return (pit::time * pit::PER_SECOND) / 10000000; }

ACPI_STATUS AcpiOsSignal(UINT32 Function, void *Info) {
  kerror("ACPICA: Signal not supported\n");
  return (AE_OK);
}

namespace {
struct PageMap {
  void *base;
  page::Page *page;

  bool operator==(const PageMap &other) const { return base == other.base && page == other.page; }
};
list<PageMap> page_mappings;
} // namespace

void *AcpiOsMapMemory(ACPI_PHYSICAL_ADDRESS PhysicalAddress, ACPI_SIZE Length) {
  Length += PAGE_SIZE - 1;
  Length /= PAGE_SIZE;
  if (Length == 0)
    Length = 1;
  Length++;

  uint32_t remainder = PhysicalAddress % PAGE_SIZE;
  PhysicalAddress -= remainder;

  page::Page *page = page::create(PhysicalAddress, page::FLAG_KERNEL, Length);
  void *addr = static_cast<void *>(page::kinstall(page, 0));
  PageMap PM{addr, page};
  page_mappings.push_back(PM);
  return reinterpret_cast<void *>(reinterpret_cast<addr_phys_t>(addr) + remainder);
}

void AcpiOsUnmapMemory(void *where, ACPI_SIZE length) {
  where = reinterpret_cast<void *>(reinterpret_cast<addr_phys_t>(where) & ~(PAGE_SIZE - 1));

  for (PageMap &PM : page_mappings) {
    if (PM.base != where)
      continue;

    page_mappings.remove(PM);
    page::kuninstall(where, PM.page);
    return;
  }
  panic("ACPICA: Could not find page_map");
}

ACPI_STATUS AcpiOsReadPort(ACPI_IO_ADDRESS Address, UINT32 *Value, UINT32 Width) {
  switch (Width) {
  case 8:
    *Value = inb(Address);
    break;
  case 32:
    *Value = inl(Address);
    break;
  default:
    panic("ACPICA: Unknown width %i", Width);
  }
  return AE_OK;
}
ACPI_STATUS AcpiOsWritePort(ACPI_IO_ADDRESS Address, UINT32 Value, UINT32 Width) {
  switch (Width) {
  case 8:
    outb(Address, Value);
    break;
  case 32:
    outl(Address, Value);
    break;
  default:
    panic("ACPICA: Unknown width %i", Width);
  }
  return AE_OK;
}

void __attribute__((format(printf, 1, 2))) AcpiOsPrintf(const char *fmt, ...) {
  va_list va;
  va_start(va, fmt);
  vprintk(fmt, va);
  va_end(va);
}

void __attribute__((format(printf, 1, 0))) AcpiOsVprintf(const char *fmt, va_list ap) { vprintk(fmt, ap); }

ACPI_STATUS AcpiOsWritePciConfiguration(ACPI_PCI_ID *PciId, UINT32 PciRegister, UINT64 Value64, UINT32 Width) {
  switch (Width) {
  case 8:
    pci::write8(PciId->Bus, PciId->Device, PciId->Function, PciRegister, Value64);
    break;
  default:
    // TODO: Other widths
    pci::write(PciId->Bus, PciId->Device, PciId->Function, PciRegister, Value64);
  }
  return AE_OK;
}

ACPI_STATUS AcpiOsReadPciConfiguration(ACPI_PCI_ID *PciId, UINT32 PciRegister, UINT64 *Value64, UINT32 Width) {
  switch (Width) {
  case 8:
    *Value64 = pci::read8(PciId->Bus, PciId->Device, PciId->Function, PciRegister);
    break;
  default:
    // TODO: Other widths
    *Value64 = pci::read(PciId->Bus, PciId->Device, PciId->Function, PciRegister);
  }
  return AE_OK;
}

void AcpiOsStall(UINT32 Microseconds) { kerror("ACPI: Stall not supported\n"); }
void AcpiOsSleep(UINT64 Milliseconds) { kerror("ACPI: Sleep not supported\n"); }
ACPI_STATUS AcpiOsExecute(ACPI_EXECUTE_TYPE Type, ACPI_OSD_EXEC_CALLBACK Function, void *Context) {
  panic("Processes not supported");
}

ACPI_STATUS AcpiOsInitialize() { return AE_OK; }
ACPI_STATUS AcpiOsTerminate() { return AE_OK; }

ACPI_STATUS AcpiOsPredefinedOverride(const ACPI_PREDEFINED_NAMES *PredefinedObject, ACPI_STRING *NewValue) {
  *NewValue = nullptr;
  return AE_OK;
}

ACPI_STATUS AcpiOsTableOverride(ACPI_TABLE_HEADER *ExistingTable, ACPI_TABLE_HEADER **NewTable) {
  *NewTable = nullptr;
  return AE_OK;
}

ACPI_STATUS AcpiOsPhysicalTableOverride(ACPI_TABLE_HEADER *ExistingTable, ACPI_PHYSICAL_ADDRESS *NewAddress,
                                        UINT32 *NewTableLength) {
  return AE_SUPPORT;
}

ACPI_PHYSICAL_ADDRESS AcpiOsGetRootPointer() {
  ACPI_PHYSICAL_ADDRESS Ret;
  Ret = 0;
  AcpiFindRootPointer(&Ret);
  return Ret;
}

void AcpiOsWaitEventsComplete() { return; }
ACPI_STATUS AcpiOsEnterSleep(UINT8 SleepState, UINT32 RegaValue, UINT32 RegbValue) { return AE_OK; }

ACPI_STATUS AcpiOsWriteMemory(ACPI_PHYSICAL_ADDRESS Address, UINT64 Value, UINT32 Width) {
  panic("Attempted to write to memory!");
}

ACPI_STATUS AcpiOsReadMemory(ACPI_PHYSICAL_ADDRESS Address, UINT64 *Value, UINT32 Width) {
  panic("Attempted to read from memory!");
}
}
