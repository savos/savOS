#include <stdint.h>

#include "int/idt.hpp"
#include "int/lapic.hpp"
#include "int/numbers.h"
#include "main/common.hpp"
#include "main/panic.hpp"
#include "mem/page.hpp"

namespace idt {
const uint32_t _IDT_LENGTH = 256;
#ifdef X86_64
const uint32_t _VECTOR_OFFSET = 0x11;
#else
const uint32_t _VECTOR_OFFSET = 0x02;
#endif
const uintptr_t _TEMPLATE_START = reinterpret_cast<uintptr_t>(idt_asm_interrupt_template);
const uintptr_t _TEMPLATE_END = reinterpret_cast<uintptr_t>(&idt_asm_interrupt_template_end);
const uintptr_t _TEMPLATE_ERR_START = reinterpret_cast<uintptr_t>(idt_asm_interrupt_template_err);
const uintptr_t _TEMPLATE_ERR_END = reinterpret_cast<uintptr_t>(&idt_asm_interrupt_template_err_end);

static volatile entry_t table[_IDT_LENGTH];
static volatile interrupt_handler_t functions[_IDT_LENGTH];
static volatile interrupt_handler_err_t functions_err[_IDT_LENGTH];
extern "C" volatile descriptor_t idt_descriptor;
volatile descriptor_t idt_descriptor = {};
static unique_ptr<page::Page> handler_table;
static char *handler_table_base;

namespace {
uintptr_t add_interrupt(uint8_t id, uintptr_t start, uintptr_t end) {
  uintptr_t handler = reinterpret_cast<uintptr_t>(handler_table_base);
  assert(!(handler % 4) && "handler is not aligned to 4 bytes!");
  size_t num = end - start;
  memcpy(reinterpret_cast<char *>(handler), reinterpret_cast<char *>(start), num);
  handler_table_base += (num + 3) & ~0x3;
  return handler;
}

void enable_entry(uint8_t vector, uinta_t offset) {
  assert(!(offset % 4) && "Offset is not aligned to 4 bytes!");
  table[vector].offset_low = (uint16_t)(offset & 0xffff);
  table[vector].offset_high = (uint16_t)(offset >> 16);
#if X86_64
  table[vector].offset_long = (uint32_t)(offset >> 32);
#endif
}

void update_entry(volatile entry_t &entry, uint16_t selector, uint8_t type_attr) {
  entry.selector = selector;
  entry.zero = 0;
  entry.type_attr = type_attr;
}

void not_set(idt_proc_state_t state, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Error when installing interrupt (vector not set)");
}
} // namespace

void install(uint8_t id, interrupt_handler_t offset, uint16_t selector, uint8_t type_attr) {
  functions[id] = offset;
  uintptr_t base = add_interrupt(id, _TEMPLATE_START, _TEMPLATE_END);
  reinterpret_cast<volatile uint8_t *>(base)[_VECTOR_OFFSET] = id;

  enable_entry(id, base);
  update_entry(table[id], selector, type_attr | FLAG_PRESENT);
}

void install_with_error(uint8_t id, interrupt_handler_err_t offset, uint16_t selector, uint8_t type_attr) {
  functions_err[id] = offset;
  uintptr_t base = add_interrupt(id, _TEMPLATE_ERR_START, _TEMPLATE_ERR_END);
  reinterpret_cast<volatile uint8_t *>(base)[_VECTOR_OFFSET] = id;

  enable_entry(id, base);
  update_entry(table[id], selector, type_attr | FLAG_PRESENT);
}

void init() {
  handler_table.reset(page::alloc(0, 1));
  handler_table_base = reinterpret_cast<char *>(page::kinstall(handler_table.get(), 0));

  idt_descriptor.size = sizeof(table) - 1;
  idt_descriptor.offset = ((uinta_t)&table);

  install(INT_NOT_SET, not_set, 0, 0);
}

void setup() {
  uinta_t idt_addr = static_cast<uinta_t>(reinterpret_cast<uintptr_t>(&idt_descriptor));
  asm volatile("lidt (%0)" : : "r"(idt_addr));
}

#if X86_64
extern "C" void idt_handle(uint32_t vector, idt_proc_state_t *state, addr_logical_t ret) {
  if (functions[vector]) {
    functions[vector](*state, ret);
  }
}

extern "C" void idt_handle_with_error(uint32_t vector, uint32_t errcode, idt_proc_state_t *state, addr_logical_t ret) {
  if (functions_err[vector]) {
    functions_err[vector](*state, errcode, ret);
  }
}
#else
extern "C" void idt_handle(uint32_t vector, idt_proc_state_t state, addr_logical_t ret) {
  if (functions[vector]) {
    functions[vector](state, ret);
  }
}

extern "C" void idt_handle_with_error(uint32_t vector, idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  if (functions_err[vector]) {
    functions_err[vector](state, errcode, ret);
  }
}
#endif
} // namespace idt
