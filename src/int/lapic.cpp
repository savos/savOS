#include <stdint.h>

#include "hw/acpi.hpp"
#include "hw/pit.hpp"
#include "hw/utils.h"
#include "int/idt.hpp"
#include "int/lapic.hpp"
#include "int/numbers.h"
#include "main/asm_utils.hpp"
#include "main/cpu.hpp"
#include "logging/printk.hpp"
#include "mem/kmem.hpp"
#include "mem/page.hpp"
#include "structures/buffer.hpp"
#include "structures/mutex.hpp"
#include "task/task.hpp"

namespace lapic {
static Buffer<volatile uint32_t> _base;
static volatile uint8_t _stage; // 0 = Waiting for first change, 1 = counting, 2 = done
static volatile uint32_t _deadline;
static volatile uint32_t _callibration_ticks;
static volatile uint32_t _ticks_per_sec;

static mutex::Mutex _command_mutex;

extern "C" volatile char _startofap;
extern "C" volatile char _endofap;
extern "C" volatile uint32_t low_ap_page_table;

static volatile bool _calibrated[MAX_CORES];

const uint32_t _CAL_INIT = 1000;
const uint32_t _CAL_DIV = 3;
const uint32_t _JUMP_BASE = 0x1000;

static uint32_t _read(uint32_t reg) { return _base[reg / sizeof(uint32_t)]; }


static void _write(uint32_t reg, uint32_t val) { _base[reg / sizeof(uint32_t)] = val; }


static void _set_timer(uint32_t timer, uint32_t divide) {
  _write(TIMER_INITIAL, timer);
  _write(TIMER_DIVIDE_CONFIGURATION, divide);
  _write(LVT_TIMER, TIMER_MODE_PERIODIC | INT_LAPIC_BASE);
}


static void _ipi(uint32_t vector, uint8_t dest, uint8_t init_deassert, uint32_t target) {
  uint32_t val = 0;
  val |= vector & 0xff;
  val |= dest << 8;
  val |= init_deassert << 14;

  _write(ICR_B, target << 24);
  _write(ICR_A, val);
  while (_read(ICR_A) & (1 << 12))
    ; // Wait until it is acked.
}

void init() {
  page::Page *page;

  page = page::create(acpi::lapic_base, page::FLAG_KERNEL, 1);
  _base =
      Buffer<volatile uint32_t>(page::kinstall(page, page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_RW), PAGE_SIZE);

  printk("LAPIC ID: %x, Version: %x\n", _read(ID), _read(VER));
  // Set the spurious interrupt vector in order to get interrupts
  _write(SPURIOUS_INT_VECTOR, 0x1ff);

  // Set up the timer
  _deadline = pit::time;
  idt::install(INT_LAPIC_BASE, timer, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  _set_timer(_CAL_INIT, _CAL_DIV);

  // And the command handler
  idt::install(INT_LAPIC_BASE + INT_LAPIC_COMMAND, handle_command, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_LAPIC_BASE + INT_LAPIC_PANIC, handle_panic, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
}


void setup() {
  _write(SPURIOUS_INT_VECTOR, 0x1ff);
  eoi();

  // Set up the timer
  _set_timer(_CAL_INIT, _CAL_DIV);
}


void timer(idt_proc_state_t state, addr_logical_t ret) {
  (void)state;

  uint32_t id = cpu::id();
  if (id == 0) {
    switch (_stage) {
    case 0:
      if (_deadline < pit::time) {
        _stage = 1;
        _deadline = pit::time + pit::PER_SECOND + 1;
        _callibration_ticks = 1;
      }
      eoi();
      break;

    case 1:
      if (_deadline < pit::time) {
        _stage = 2;
        _ticks_per_sec = _callibration_ticks * _CAL_INIT * _CAL_DIV;
        _write(TIMER_INITIAL, _ticks_per_sec / _CAL_DIV / SWITCHES_PER_SECOND);
        _callibration_ticks = 0;
      } else {
        _callibration_ticks++;
      }
      eoi();
      break;

    case 2:
      eoi();
      task::task_timer_yield();
      break;

    default:
      eoi();
      break;
    }
  } else {
    if (_calibrated[id]) {
      eoi();
      task::task_timer_yield();
    } else {
      if (_ticks_per_sec) {
        _set_timer(_ticks_per_sec / _CAL_DIV / SWITCHES_PER_SECOND, _CAL_DIV);
        _write(TIMER_INITIAL, _ticks_per_sec / _CAL_DIV / SWITCHES_PER_SECOND);
        _calibrated[id] = true;
      } else {
        // Do nothing
      }
      eoi();
    }
  }
}


void awaken_others() {
#if X86_64
#else
  page::Page *page = page::create(_JUMP_BASE, page::FLAG_KERNEL, 1);
  uint8_t *jump_base = static_cast<uint8_t *>(page::kinstall(page, 0));

  // Copy data into the jump start point thing
  for (uint32_t i = 0; i < (uinta_t)&_endofap - (uinta_t)&_startofap; i++) {
    jump_base[i] = *(&_startofap + i);
  }

  *reinterpret_cast<uint32_t *>(KERNEL_VM_BASE + reinterpret_cast<addr_phys_t>(&low_ap_page_table)) =
      kmem::map.vm_start - KERNEL_VM_BASE;
#endif

  // Loop through and wake them all up (except number 0)
  for (size_t i = 1; i < acpi::proc_count && i < MAX_CORES; i++) {
    uint32_t id = acpi::procs[i].apic_id;

    uint32_t eflags = push_cli();
    _ipi(0, 5, 1, id);
    io_wait();
    _ipi((uint32_t)_JUMP_BASE / PAGE_SIZE, 6, 1, id);
    io_wait();
    _ipi((uint32_t)_JUMP_BASE / PAGE_SIZE, 6, 1, id);
    pop_flags(eflags);

    while (!cpu::info_of(id).awoken)
      ;
  }
#if X86_64
#else
  page::kuninstall(jump_base, page);
#endif
}


void eoi() { _write(EOI, 0xffffffff); }


void ipi(uint8_t vector, uint32_t proc) {
  // TODO: LAPIC CPU ids probably don't equal savos CPU ids
  _ipi(vector, 0, 0, proc);
}

void ipi_all(uint8_t vector) {
  for (uint32_t i = 0; i < acpi::proc_count && i < MAX_CORES; i++) {
    ipi(vector, i);
  }
}

void send_command(Command command, uint32_t argument, uint32_t proc) {
  LockGuard guard{_command_mutex};
  cpu::Status &status = cpu::info_of(proc);
  if (!status.awoken)
    panic("Tried to send command to a CPU that is not awoken!");
  status.command_finished = 0;
  status.command = command;
  status.command_arg = argument;
  ipi(INT_LAPIC_BASE + INT_LAPIC_COMMAND, proc);
  while (!status.command_finished) {
  };
}

void send_command_all(Command command, uint32_t argument) {
  // TODO: This better
  uint32_t eflags = push_cli();
  uint32_t id = cpu::id();
  for (uint32_t i = 0; i < acpi::proc_count; i++) {
    if (!cpu::info_of(i).awoken)
      continue;
    if (i != id) {
      send_command(command, argument, i);
    }
  }

  pop_flags(eflags);
}

void handle_command(idt_proc_state_t state, addr_logical_t ret) {
  cpu::Status &status = cpu::info();

  switch (status.command) {
  case Command::invlpg:
    asm volatile("invlpg (%0)" : : "r"(status.command_arg));
    break;

  default:
    panic("Unknown IPI command 0x%x", status.command);
  }

  status.command_finished = true;
  eoi();
}

void handle_panic(idt_proc_state_t state, addr_logical_t ret) {
  eoi();

  while (true) {
    asm volatile("hlt");
  }
}
} // namespace lapic
