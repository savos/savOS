#include "logging/log.hpp"
#include "display/logdisplay.hpp"

namespace log {
namespace {
LogEntry entry = {};

const size_t BUFFER_SIZE = 128;
char buffer[BUFFER_SIZE + 1] = {};
size_t buff_p = 0;
} // namespace

error_t LogStream::write(const void *in_buff, size_t len, uint32_t flags, void *data, uint32_t *written) {
  auto append = [&]() {
    auto tmp_buffer = new char[buff_p + 1];
    strcpy(tmp_buffer, buffer);
    entry.message = entry.message + tmp_buffer;
    buff_p = 0;
  };

  for (size_t i = 0; i < len; i++) {
    buffer[buff_p] = reinterpret_cast<const char *>(in_buff)[i];

    if (buffer[buff_p] == '\n') {
      // Commit this log entry
      buffer[buff_p] = '\0';

      append();
      entry.flag = flags;
      entries.push_back(entry);
      entry = {};

      buff_p = 0;
    } else if (buff_p == BUFFER_SIZE - 1) {
      // Out of temporary buffer
      append();
    } else {
      buff_p++;
    }
  }

  *written = len;
  return EOK;
}

LogStream stream;
vector<LogEntry> entries;
} // namespace log
