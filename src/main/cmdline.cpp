#include "main/cmdline.hpp"

namespace cmdline {

enum class Mode {
  entry,
  key,
  value,
  quote_value,
};

vector<OptionPair> options;

void parse(char *line) {
  char *p = line;
  char *start = 0;
  Mode mode = Mode::entry;
  OptionPair current;

  auto is_ws = [&]() { return *p == ' ' || *p == '\t' || *p == '\n'; };
  auto skip_ws = [&]() {
    while (is_ws())
      p++;
  };
  auto create = [&]() {
    assert(start <= p - 1);
    size_t len = p - start;

    char *value = new char[len + 1];
    memcpy(value, start, len);
    value[len] = '\0';

    auto to_return = Utf8::own(value, len);
    return to_return;
  };
  auto push = [&]() { options.push_back(current); };

  while (*p) {
    switch (mode) {
    case Mode::entry:
      skip_ws();
      start = p;
      mode = Mode::key;
      break;

    case Mode::key:
      if (is_ws() || *p == '=') {
        // Reached the end, look for the equals now
        current.name = create();
        skip_ws();

        if (*p == '=') {
          // `option=value` form
          p++;
          skip_ws();
          if (*p == '"') {
            mode = Mode::quote_value;
            p++;
            start = p;
          } else {
            mode = Mode::value;
            start = p;
          }
        } else {
          // `option` form
          current.value = "1";
          push();
          mode = Mode::entry;
        }
      } else {
        // Continue consuming the key
        p++;
      }
      break;

    case Mode::value:
      if (is_ws()) {
        // Reached the end of the value
        current.value = create();
        push();
        mode = Mode::entry;
      } else {
        p++;
      }
      break;

    case Mode::quote_value:
      // NOTE: \\ is not supported
      if (*p == '"' && *(p - 1) != '\\') {
        // Reached the end of the value
        current.value = create();
        push();
        p++;
        mode = Mode::entry;
      } else {
        p++;
      }
      break;
    }
  }

  if (mode == Mode::key) {
    // If a `option` format (with no `=whatever`) part is at the end of the string
    current.name = create();
    current.value = "1";
    push();
  }

  if (mode == Mode::value) {
    // If a `option=whatever` value is at the end of the string
    current.value = create();
    push();
  }
}

Utf8 get_option_string(char *name, Utf8 def) {
  for (auto &o : options) {
    if (o.name == name) {
      return o.value;
    }
  }
  return def;
}

} // namespace cmdline
