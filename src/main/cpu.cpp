#include <stdint.h>

#include "hw/acpi.hpp"
#include "main/asm_utils.hpp"
#include "main/cpu.hpp"
#include "main/panic.hpp"
#include "mem/page.hpp"
#include "structures/shared_ptr.hpp"
#include "structures/unique_ptr.hpp"
#include "task/task.hpp"

/** @file main/cpu.c
 *
 * Creates the CPU status for the (as of yet) only CPU.
 *
 * When `cpu_init` is called, the processor is given a stack and set up correctly.
 */

namespace cpu {
static unique_ptr<Status> cpu_status[MAX_CORES]; // Enough space for as many processors as we can get

extern "C" addr_logical_t *stacks[MAX_CORES];
addr_logical_t *stacks[MAX_CORES] = {};
bool inited = false;

uint32_t id() {
  CHECK_IF_CLR;
  uint32_t volatile id;

  __asm__ volatile("\
            mov $1, %%eax\n\
            cpuid\n\
            shr $24, %%ebx\n\
            mov %%ebx, %0"
                   : "=r"(id)
                   : /* Nothing */
                   : "eax", "ebx", "ecx", "edx");

  return id;
}

Status &info() { return info_of(id()); }

Status &info_of(uint32_t id) {
  assert(inited && "CPU system is not inited");
  if (!cpu_status[id]) {
    panic("Tried to get the status of an invalid or un-inited cpu (%d)", id);
  }
  return *cpu_status[id];
}

void init() {
  page::Page *page;
  assert(acpi::inited);
  uint32_t cores = acpi::proc_count;
  if (!acpi::acpi_found)
    cores = 1;

  for (uint32_t i = 0; i < cores; i++) {
#if X86_64
    if (i == 0) {
      page = page::create(loader_data::passed_loader_data.stack_page, 0, 1);
    } else {
      page = page::alloc(0, 1);
    }
#else
    page = page::alloc(0, 1);
#endif
    cpu_status[i] = make_unique<Status>();
    cpu_status[i]->cpu_id = i;
    cpu_status[i]->stack = page::kinstall(page, page::PAGE_TABLE_RW);
    cpu_status[i]->awoken = false;
    stacks[i] = (addr_logical_t *)(cpu_status[i]->stack);
    cpu_status[i]->thread = NULL;
  }

  cpu_status[0]->awoken = true;
#if X86_64
  // Swap over the stack pointer
  uint64_t rsp;
  asm("mov %%rsp, %0" : "=r"(rsp));
  rsp &= (PAGE_SIZE - 1);
  rsp += reinterpret_cast<uint64_t>(cpu_status[0]->stack);
  asm("mov %0, %%rsp" ::"r"(rsp));
#endif
  inited = true;
}

shared_ptr<task::Thread> current_thread() {
  shared_ptr<task::Thread> t;
  uint32_t eflags = push_cli();
  t = cpu_status[id()]->thread;
  pop_flags(eflags);
  return t;
}

shared_ptr<task::Thread> current_thread_noint() {
  CHECK_IF_CLR;
  return cpu_status[id()]->thread;
}
} // namespace cpu
