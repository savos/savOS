#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "debug/assert.hpp"
#include "main/utils.h"

extern "C" {
void *memcpy(void *destination, const void *source, size_t num) {
  assert(destination);
  assert(source);
  for (size_t i = 0; i < num; i++) {
    ((char *)destination)[i] = ((char *)source)[i];
  }
  return destination;
}

size_t strlen(const char *str) {
  assert(str);
  size_t p = 0;
  while (p < UINT32_MAX) {
    if (str[p] == '\0') {
      return p;
    }
    p++;
  }
  return UINT32_MAX - 1;
}

void *memset(void *ptr, int value, size_t num) {
  assert(ptr);
  for (size_t i = 0; i < num; i++) {
    reinterpret_cast<char *>(ptr)[i] = static_cast<unsigned char>(value);
  }
  return ptr;
}

volatile void *memset_v(volatile void *ptr, int value, size_t num) {
  assert(ptr);
  for (size_t i = 0; i < num; i++) {
    reinterpret_cast<volatile char *>(ptr)[i] = static_cast<unsigned char>(value);
  }
  return ptr;
}

char *strcat(char *destination, const char *source) {
  assert(destination);
  assert(source);
  char *to_ret = destination;
  while (*(destination++))
    ;
  destination--;
  while ((*(destination++) = *(source++)))
    ;
  return to_ret;
}

char *strncat(char *destination, const char *source, size_t num) {
  assert(destination);
  assert(source);
  char *to_ret = destination;
  size_t total = num;
  while (*(destination++) && num)
    num--;
  destination--;
  while ((*(destination++) = *(source++)) && num)
    num--;
  to_ret[total] = 0;
  return to_ret;
}

int strcmp(const char *str1, const char *str2) {
  while (*str1 || *str2) {
    if (*str1 < *str2)
      return -1;
    if (*str1 > *str2)
      return 1;
    str1++;
    str2++;
  }
  return 0;
}

int strncmp(const char *str1, const char *str2, size_t num) {
  while ((*str1 || *str2) && num--) {
    if (*str1 < *str2)
      return -1;
    if (*str1 > *str2)
      return 1;
    str1++;
    str2++;
  }
  return 0;
}

char *strcpy(char *destination, const char *source) {
  char *to_return = destination;
  while ((*(destination++) = *(source++)))
    ;
  return to_return;
}

char *strncpy(char *destination, const char *source, size_t num) {
  for (size_t i = 0; i < num; i++) {
    if (!*source) {
      destination[i] = 0;
    } else {
      destination[i] = *source;
      source++;
    }
  }
  return destination;
}

int memcmp(const void *ptr1, const void *ptr2, size_t num) {
  const uint8_t *cptr1 = reinterpret_cast<const uint8_t *>(ptr1);
  const uint8_t *cptr2 = reinterpret_cast<const uint8_t *>(ptr2);
  while (num--) {
    if (*cptr1 < *cptr2)
      return -1;
    if (*cptr1 > *cptr2)
      return 1;
    cptr1++;
    cptr2++;
  }
  return 0;
}

int tolower(int c) {
  if (c >= 'A' && c <= 'Z') {
    return (c - 'A') + 'a';
  }
  return c;
}
int toupper(int c) {
  if (c >= 'a' && c <= 'z') {
    return (c - 'a') + 'A';
  }
  return c;
}
int isprint(int c) {
  if (c > 0x1f && c != 0x7f)
    return 1;
  return 0;
}
int isdigit(int c) {
  if (c >= '0' && c <= '9')
    return 1;
  return 0;
}
int isxdigit(int c) {
  if (c >= '0' && c <= '9')
    return 1;
  if (c >= 'a' && c <= 'f')
    return 1;
  if (c >= 'A' && c <= 'F')
    return 1;
  return 0;
}
int isspace(int c) {
  if (c == ' ' || (c >= 0x09 && c <= 0x0d))
    return 1;
  return 0;
}
}
