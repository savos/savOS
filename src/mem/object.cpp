#include <stddef.h>
#include <stdint.h>

#include "logging/printk.hpp"
#include "main/cpu.hpp"
#include "main/panic.hpp"
#include "mem/kmem.hpp"
#include "mem/object.hpp"
#include "mem/page.hpp"
#include "mem/vm.hpp"
#include "test/test.hpp"

namespace object {
/**
 * @todo Handle page flags
 */

Object::Object(uint32_t max_pages, uint8_t page_flags, uint8_t object_flags, uint32_t offset)
    : max_pages(max_pages), page_flags(page_flags), object_flags(object_flags) {}

Object::~Object() {}

// count is in pages, Addr is an address not in pages
bool Object::generate(uint32_t addr, uint32_t count) {
  PageEntry *next = nullptr;
  PageEntry *prev = nullptr;

  // If we would go greater than the maximum number of pages, cap it
  // TODO: Do I really want to do this?
  if ((addr / PAGE_SIZE) + count > max_pages) {
    if (addr / PAGE_SIZE > max_pages) {
      // It is out of range, abort
      return false;
    }
    count = max_pages - (addr / PAGE_SIZE);
  }

  // Find the location to insert the page into
  for (next = pages.get(); next && next->offset < addr; (prev = next), (next = next->next.get()))
    ;

  // If we are bumping into the next block of pages, then reduce the amount of pages to load appropriately
  if (next && addr + (count * PAGE_SIZE) >= next->offset) {
    count = (next->offset - addr) / PAGE_SIZE;
  }

  if (!count)
    return false;

  // Generate the pages
  page::PageRaii page = do_generate(addr, count);
  unique_ptr<PageEntry> new_entry;

  // And create a new entry
  page::Page *page_ptr = page.get_page();
  new_entry = make_unique<PageEntry>();
  new_entry->offset = addr;
  new_entry->page = move(page);
  new_entry->next = next;
  if (prev) {
    prev->next.release();
    prev->next = move(new_entry);
  } else {
    pages.release();
    pages = move(new_entry);
  }

  // Now update the tables
  for (ObjectInMap *oim : objects_in_maps) {
    oim->map->insert(oim->base + addr - oim->offset, page_ptr, page_flags, oim->base,
                     oim->base + oim->pages * PAGE_SIZE);
  }
  return true;
}

Failable<PageAndOffset> Object::lookup_addr(addr_logical_t addr) const {
  addr = addr & ~(PAGE_SIZE - 1);

  for (PageEntry *p = pages.get(); p; p = p->next.get()) {
    addr_logical_t top = p->offset + p->page->count() * PAGE_SIZE;
    if (p->offset <= addr && top > addr) {
      return Failable<PageAndOffset>({p->page, (addr - p->offset) / PAGE_SIZE});
    }
  }

  return Failable<PageAndOffset>(ENOENT);
}

void Object::add_object_in_map(ObjectInMap *oim) {
  objects_in_maps.push_front(oim);

  vm::BaseMap *map = oim->map;

  // Fill in the pages already loaded into the vm
  for (PageEntry *page_entry = pages.get(); page_entry; page_entry = page_entry->next.get()) {
    map->insert(oim->base + page_entry->offset - oim->offset, page_entry->page.get_page(), this->page_flags, oim->base,
                oim->base + oim->pages * PAGE_SIZE);
  }
}

void Object::remove_object_in_map(ObjectInMap *oim) {
  PageEntry *page_entry;

  // Erase all the page table entries
  for (page_entry = pages.get(); page_entry; page_entry = page_entry->next.get()) {
    oim->map->clear(oim->base + page_entry->offset - oim->offset, page_entry->page->count());
  }

  objects_in_maps.remove(oim);
}


ObjectInMap::ObjectInMap(shared_ptr<Object> object, vm::BaseMap *map, uint32_t base, int64_t offset, uint32_t pages)
    : object(object), map(map), base(base), offset(offset), pages(pages) {
  object->add_object_in_map(this);
}

ObjectInMap::~ObjectInMap() { object->remove_object_in_map(this); }


page::PageRaii EmptyObject::do_generate(addr_logical_t addr, uint32_t count) {
  (void)addr;
  return page::PageRaii(0, count);
}
} // namespace object


namespace _tests {
class ObjectTest : public test::TestCase {
public:
  ObjectTest() : test::TestCase("Kernel Object Test"){};

  class IncObject : public object::Object {
  public:
    IncObject(uint32_t max_pages, uint8_t page_flags, uint8_t object_flags, uint32_t offset)
        : Object(max_pages, page_flags, object_flags, offset){};

    page::PageRaii do_generate(addr_logical_t addr, uint32_t count) {
      (void)addr;
      page::PageRaii p(0, count);

      p.install(0);
      uint32_t *installed = p.data<uint32_t>();

      for (uint32_t i = 0; i < count * PAGE_SIZE / 4; i++) {
        installed[i] = addr + (i * 4);
      }

      p.uninstall();

      return p;
    }
  };

  void run_test() override {
    using namespace object;

    test("Creating a simple object");
    shared_ptr<Object> obj = make_shared<IncObject>(5, page::PAGE_TABLE_RW, 0, 0);

    test("Installing an object");
    vm::BaseMap *map = cpu::current_thread()->vm.get();

    map->add_object(obj, 0x2000, 0x0, 2);
    tassert(*(int *)(0x2000) == 0x0);
    tassert(*(int *)(0x2004) == 0x4);
    tassert(*(int *)(0x3004) == 0x1004);

    test("Installing an object with offset");
    map->add_object(obj, 0x4000, 0x2000, 2);
    tassert(*(int *)(0x4000) == 0x2000);
    tassert(*(int *)(0x4004) == 0x2004);
    tassert(*(int *)(0x5004) == 0x3004);

    test("Loading an already populated object");
    map->add_object(obj, 0x6000, 0x1000, 4);
    tassert(*(int *)(0x6000) == 0x1000);
    tassert(*(int *)(0x7000) == 0x2000);
    tassert(*(int *)(0x8000) == 0x3000);

    test("Removing an object");
    map->remove_object(obj);
  }
};

test::AddTestCase<ObjectTest> objectTest;
} // namespace _tests
