#include <stddef.h>
#include <stdint.h>

#include "int/lapic.hpp"
#include "logging/printk.hpp"
#include "main/asm_utils.hpp"
#include "main/cpu.hpp"
#include "main/panic.hpp"
#include "mem/kmem.hpp"
#include "mem/object.hpp"
#include "mem/page.hpp"
#include "mem/vm.hpp"
#include "structures/mutex.hpp"

namespace vm {
mutex::Mutex _mutex;

BaseMap::BaseMap(uint32_t pid, uint32_t task_id, bool kernel) : pid(pid), task_id(task_id) {}
width32::Map::Map(uint32_t pid, uint32_t task_id, bool kernel) : BaseMap(pid, task_id, kernel) {
  size_t i;

  // Load the kernel tables into it
  for (i = 0; i < PAGE_TABLE_LENGTH; i++) {
    if (i >= PAGE_TABLE_LENGTH - KERNEL_VM_PAGE_TABLES) {
      // logical_dir->entries[i] = page::page_dir->entries[i];
      directory.set_raw_nochildren(i * PAGE_DIR_SIZE, page::page_dir->entries[i]);
    } else {
      // logical_dir->entries[i] = 0;
    }
  }
}
width64::Map::Map(uint32_t pid, uint32_t task_id, bool kernel) : BaseMap(pid, task_id, kernel) {
  pml4.set_raw_nochildren(KERNEL_VM_BASE_64, loader_data::passed_loader_data.pdp_high_page | page::PAGE_TABLE_PRESENT);
}

BaseMap::~BaseMap() {}
width32::Map::~Map() {
  // Remove all the objects
  objects_in_maps.clear();
}
width64::Map::~Map() {
  // Remove all the objects
  objects_in_maps.clear();
}


void width32::Map::insert(int64_t addr, page::Page *page, uint8_t page_flags, uint32_t min, uint32_t max) {
  if (addr >= max || addr < 0) {
    return;
  }

  for (uint32_t i = 0; i < page->consecutive; i++) {
    if (addr >= min) {
      directory.set(addr, page->get_mem_base() + i * PAGE_SIZE, page_flags | page::PAGE_TABLE_PRESENT);
      invlpg(addr);
    }

    addr += PAGE_SIZE;

    if (addr >= max) {
      return;
    }
  }

  if (page->next) {
    insert(addr, page->next, page_flags, min, max);
  }
}
void width64::Map::insert(int64_t addr, page::Page *page, uint8_t page_flags, uint32_t min, uint32_t max) {
  if (addr >= max || addr < 0) {
    return;
  }

  for (uint32_t i = 0; i < page->consecutive; i++) {
    if (addr >= min) {
      pml4.set(addr, page->get_mem_base() + i * PAGE_SIZE, page_flags | page::PAGE_TABLE_PRESENT);
      invlpg(addr);
    }

    addr += PAGE_SIZE;

    if (addr >= max) {
      return;
    }
  }

  if (page->next) {
    insert(addr, page->next, page_flags, min, max);
  }
}


void width32::Map::clear(int64_t addr, uint32_t pages) {
  if (addr < 0)
    return;

  for (uint32_t i = 0; i < pages; i++) {
    directory.clear(addr);
    invlpg(addr);

    addr += PAGE_SIZE;
  }
}
void width64::Map::clear(int64_t addr, uint32_t pages) {
  if (addr < 0)
    return;

  for (uint32_t i = 0; i < pages; i++) {
    pml4.clear(addr);
    invlpg(addr);

    addr += PAGE_SIZE;
  }
}


bool BaseMap::resolve_fault(addr_logical_t addr) {
  asm volatile("sti");

  // Loop through the objects and see if any fit
  for (unique_ptr<object::ObjectInMap> &oim : objects_in_maps) {
    if (addr >= oim->base && addr < oim->base + oim->pages * PAGE_SIZE) {
      // OK!
      uint32_t excess = addr % PAGE_SIZE;
      if (oim->object->generate(addr - oim->base + oim->offset - excess, 1)) {
        return true;
      }
    }
  }

  return false;
}


void BaseMap::add_object(const shared_ptr<object::Object> &object, uint32_t base, int64_t offset, uint32_t pages) {
  unique_ptr<object::ObjectInMap> oim = make_unique<object::ObjectInMap>(object, this, base, offset, pages);

  objects_in_maps.push_front(move(oim));
}

void BaseMap::remove_object(const shared_ptr<object::Object> &object) {
  for (auto i = objects_in_maps.begin(); i != objects_in_maps.end();) {
    if ((*i)->object == object) {
      i = objects_in_maps.erase(i);
    } else {
      i++;
    }
  }
}

void BaseMap::remove_object_at(const shared_ptr<object::Object> &object, uint32_t base) {
  for (auto i = objects_in_maps.begin(); i != objects_in_maps.end();) {
    if ((*i)->object == object && (*i)->base == base) {
      i = objects_in_maps.erase(i);
      break;
    } else {
      i++;
    }
  }
}

void width32::Map::enter() {
  LockGuard guard{_mutex};
  if (using_cpu != 0xffffffff) {
    panic("Tried to set a VM which is already owned by another CPU");
  }
  using_cpu = cpu::id();
  __asm__ volatile("mov %0, %%cr3" : : "r"(directory.get_addr()));
}
void width64::Map::enter() {
  LockGuard guard{_mutex};
  if (using_cpu != 0xffffffff) {
    panic("Tried to set a VM which is already owned by another CPU");
  }
  using_cpu = cpu::id();
  __asm__ volatile("mov %0, %%cr3" : : "r"(pml4.get_addr()));
}

void BaseMap::exit() {
  LockGuard guard{_mutex};
  using_cpu = 0xffffffff;
#if X86_64
  __asm__ volatile("mov %0, %%cr3" : : "r"(loader_data::passed_loader_data.pml4_page));
#else
  __asm__ volatile("mov %0, %%cr3" : : "r"((addr_phys_t)page::page_dir - KERNEL_VM_BASE));
#endif
}

void BaseMap::invlpg(addr_logical_t addr) {
  uint32_t eflags = push_cli();
  _mutex.lock();
  if (using_cpu != 0xffffffff) {
    if (using_cpu == cpu::id()) {
      __asm__ volatile("invlpg (%0)" : : "r"(addr));
    } else {
      lapic::send_command(lapic::Command::invlpg, addr, using_cpu);
    }
  }
  _mutex.unlock();
  pop_flags(eflags);
}
} // namespace vm
