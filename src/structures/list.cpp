#include "main/common.hpp"
#include "test/test.hpp"

namespace _tests {
class ListTest : public test::TestCase {
public:
  ListTest() : test::TestCase("List Test"){};

  virtual void run_test() override {
    test("Constructors");
    list<int> a = list<int>();
    tassert(a.size() == 0);
    tassert(a.empty());

    list<int> b = list<int>(5);
    tassert(b.size() == 5);
    tassert(b.front() == 0);
    tassert(!b.empty());

    test("Emptying a List");
    b.clear();
    tassert(b.size() == 0);
    tassert(b.empty());

    test("Front pushing a list");
    b.push_front(1);
    tassert(b.size() == 1);
    tassert(!b.empty());
    tassert(b.front() == 1);
    tassert(b.back() == 1);

    b.push_front(2);
    tassert(b.size() == 2);
    tassert(b.front() == 2);
    tassert(b.back() == 1);

    test("Front popping a list");
    b.pop_front();
    tassert(b.size() == 1);
    tassert(b.front() == 1);
    tassert(b.back() == 1);

    b.pop_front();
    tassert(b.empty());

    test("Back pushing a list");
    b.push_back(1);
    tassert(b.size() == 1);
    tassert(!b.empty());
    tassert(b.front() == 1);
    tassert(b.back() == 1);

    b.push_back(2);
    tassert(b.size() == 2);
    tassert(b.front() == 1);
    tassert(b.back() == 2);

    test("Back popping a list");
    b.push_back(2);
    b.push_back(3);
    b.pop_back();
    tassert(b.size() == 3);
    tassert(b.front() == 1);
    tassert(b.back() == 2);

    b.pop_back();
    b.pop_back();
    b.pop_back();
    tassert(b.empty());
    tassert(b.size() == 0);

    test("Iterating through a list");
    b.push_front(1);
    b.push_front(2);
    b.push_front(3);
    b.push_front(4);
    b.push_front(5);

    uint8_t seeking = 5;
    for (int e : b) {
      tassert(e == (seeking--));
    }

    test("Removing an entry");
    b.remove(1);
    tassert(b.size() == 4);
    tassert(b.front() == 5);
    tassert(b.back() == 2);

    b.remove(5);
    tassert(b.size() == 3);
    tassert(b.front() == 4);
    tassert(b.back() == 2);

    test("Erase");
    for (auto i = b.begin(); i != b.end();) {
      if (*i == 3) {
        i = b.erase(i);
      } else {
        i++;
      }
    }
    tassert(b.size() == 2);
    tassert(b.front() == 4);
    tassert(b.back() == 2);

    for (auto i = b.begin(); i != b.end();) {
      if (*i == 2) {
        i = b.erase(i);
      } else {
        i++;
      }
    }
    tassert(b.size() == 1);
    tassert(b.front() == 4);
    tassert(b.back() == 4);

    for (auto i = b.begin(); i != b.end();) {
      if (*i == 4) {
        i = b.erase(i);
      } else {
        i++;
      }
    }
    tassert(b.empty());

    test("Emplace");
    uint8_t constructs = 0;
    class TestClass {
    public:
      int val;
      TestClass(int i, uint8_t &const_count) : val(i) { const_count++; }
    };

    list<TestClass> c;
    c.emplace_back(1, constructs);
    c.emplace_front(2, constructs);

    seeking = 2;
    for (TestClass e : c) {
      tassert(e.val == (seeking--));
    }

    c.clear();
    tassert(c.empty());
    tassert(constructs == 2);
  }
};

test::AddTestCase<ListTest> listTest;
} // namespace _tests
