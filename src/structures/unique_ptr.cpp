#include "structures/unique_ptr.hpp"
#include "test/test.hpp"

namespace _tests {
class UniquePtrTest : public test::TestCase {
public:
  UniquePtrTest() : test::TestCase("unique_ptr Test"){};

  void run_test() override {
    test("Constructing");

    int *a = new int(7);
    unique_ptr<int> ap = unique_ptr<int>(a);

    tassert(ap);
    tassert(*ap == 7);

    test("Moving a pointer");
    unique_ptr<int> bp = unique_ptr<int>(move(ap));
    tassert(bp);
    tassert(!ap);
    tassert(*bp == 7);

    ap = move(bp);
    tassert(ap);
    tassert(!bp);
    tassert(*ap == 7);

    test("Releasing a pointer");
    tassert(ap.release() == a);
    tassert(!ap);

    test("Resetting a pointer");
    int *b = new int(8);
    ap.reset(b);
    tassert(*ap == 8);

    bp.reset(a);
    tassert(*bp == 7);

    test("Swapping pointers");
    ap.swap(bp);
    tassert(*bp == 8);
    tassert(*ap == 7);

    bp = nullptr;
    ap.swap(bp);
    tassert(!ap);
    tassert(*bp == 7);

    ap.swap(bp);
    tassert(!bp);
    tassert(*ap == 7);

    test("make_unique");
    bp = make_unique<int>(9);
    tassert(*bp == 9);
  }
};

test::AddTestCase<UniquePtrTest> uniquePtrTest;
} // namespace _tests
